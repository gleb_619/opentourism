package org.open.ui.view.mydashboard.ui;

/**
 * DISCLAIMER
 * 
 * The quality of the code is such that you should not copy any of it as best
 * practice how to build Vaadin applications.
 * 
 * @author jouni@vaadin.com
 * 
 */

import javax.servlet.annotation.WebServlet;

import org.open.tourism.util.ContentType;
import org.open.tourism.util.Domain;
import org.test.ui.view.mydashboard.component.ContentArea;
import org.test.ui.view.mydashboard.component.CustomMenuLayout;
import org.test.ui.view.mydashboard.component.LogoArea;
import org.test.ui.view.mydashboard.component.LogoLayout;
import org.test.ui.view.mydashboard.component.MainLayout;
import org.test.ui.view.mydashboard.component.SidebarArea;
import org.test.ui.view.mydashboard.component.UserLayout;
import org.test.ui.view.mydashboard.ui.factory.DefaultDomainFactory;
import org.test.ui.view.mydashboard.view.MyDash;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.UI;

@Theme("dashboard")
@Title("QuickTickets MyDashboard")
public class MyDashboardUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = MyDashboardUI.class, heartbeatInterval = 600)
	public static class Servlet extends VaadinServlet {

		private static final long serialVersionUID = -2044040951274969792L;

	}

	private static final long serialVersionUID = 791802811704719403L;
	private ContentArea contentArea;
	private LogoArea logoArea;
	private LogoLayout logoLayout;
	private MainLayout mainLayout;
	private Component menuLayout;
	private Navigator nav;
	private Boolean navSettings = true;
	private CssLayout root = new CssLayout();
	private SidebarArea sidebarArea;

	private UserLayout userLayout;

	private void buildContent() {
		/*
		 * TabSheet tabSheet = new TabSheet(); tabSheet.setSizeFull();
		 * 
		 * TabView<UIMenu> tabView = new TabView<UIMenu>(UIMenu.class).init();
		 * Tab tab = tabSheet.addTab(tabView);
		 * tab.setCaption(UIMenu.class.getSimpleName());
		 */

		contentArea = new ContentArea(new MyDash().init()).init();
		nav = new Navigator(this, contentArea);

		setContent(root);
		root.addStyleName("root");
		root.setSizeFull();

		if (logoArea == null) {
			logoArea = new LogoArea("Open Tourism", "Easy tourism.").init();

			logoArea.getMenuItem().addClickListener((e) -> {
				sidebarArea.collapseIt();
			});
		}

		if (logoLayout == null) {
			logoLayout = new LogoLayout(logoArea, "img/palm_64.png").init();
		}

		if (menuLayout == null) {
			menuLayout = new CustomMenuLayout(nav).init();
//			menuLayout = new MenuLayout(menuContainer, nav).init();
		}

		// menuLayout.addStyleName("menu-hidden");
		// menuLayout.addStyleName("new-menu");

		if (userLayout == null) {
			userLayout = new UserLayout("Walles", "Gromit").init();
		}

		if (sidebarArea == null) {
			sidebarArea = new SidebarArea(logoLayout, menuLayout, userLayout)
					.init();
		}

		if (mainLayout == null) {
			mainLayout = new MainLayout(sidebarArea, contentArea).init();
			root.addComponent(mainLayout);
		}

		if (navSettings) {
			navigateSettings();
		}
	}

	@Override
	protected void init(VaadinRequest request) {
		// TODO Auto-generated method stub
		getSession().setConverterFactory(new DefaultDomainFactory());
		setLocale(Page.getCurrent().getWebBrowser().getLocale());
		/*
		 * if (menuContainer == null) { menuContainer =
		 * JPAContainerFactory.make(UIMenu.class,
		 * PersistenceConfUtil.PERSISTENCE_UNIT_NAME);
		 * 
		 * // menuContainer.sort(new String[] { "id" }, new boolean[] { false //
		 * }); }
		 */
		buildContent();

		/*
		 * String f = Page.getCurrent().getUriFragment(); if (f != null &&
		 * f.startsWith("!")) { f = f.substring(1); }
		 * 
		 * if (f == null || f.equals("") || f.equals("/")) {
		 * nav.navigateTo("/uimenu"); } else { nav.navigateTo(f); }
		 */

	}

	private void navigateSettings() {
		navSettings = false;
		
		nav.addView("deal", ContentType.getViewByCode(Domain.DEAL, ContentType.PAGEVIEW.toString()));
		
		nav.setErrorView(MyDash.class);
		
/*
		menuContainer
				.getItemIds()
				.stream()
				.forEach(
						(it) -> {

							EntityItem<?> entityItem = menuContainer
									.getItem(it);

							String content_type = entityItem.getItemProperty(
									"content_type").getValue() == null ? ""
									: entityItem
											.getItemProperty("content_type")
											.getValue().toString();

							String regex = entityItem
									.getItemProperty("content").getValue() == null ? ""
									: entityItem.getItemProperty("content")
											.getValue().toString();

							String childrenContentType = entityItem
									.getItemProperty("children_content_type")
									.getValue() == null ? "TabView"
									: entityItem
											.getItemProperty(
													"children_content_type")
											.getValue().toString();

							String link = entityItem.getItemProperty("link")
									.getValue() == null ? "/" : entityItem
									.getItemProperty("link").getValue()
									.toString();

							if (ContentType.contains(content_type)) {

								String entity = entityItem.getItemProperty(
										"entity").getValue() == null ? "ROOT"
										: entityItem.getItemProperty("entity")
												.getValue().toString();

								nav.addView(link,

								(View) ContentType.getViewByCode(
										Domain.valueOf(entity.toUpperCase()),
										content_type.toUpperCase(), regex,
										childrenContentType));

							} else {

								nav.addView(entityItem.getItemProperty("link")
										.getValue().toString(),
										DefaultView.class);
							}
						});

		nav.setErrorView(MyDash.class);
*/
	}
}
