package org.open.tourism.service;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.eclipse.persistence.jpa.JpaHelper;
import org.eclipse.persistence.queries.QueryByExamplePolicy;
import org.eclipse.persistence.queries.ReadObjectQuery;
import org.open.tourism.util.PersistenceConfUtil;

public class MainService<T> {

	public static final Logger log = Logger.getLogger(MainService.class);

	// public static final Log log = LogFactory.getLog(MainService.class);

	private EntityManager entityManager;

	private T instance;

	private List<T> instances;

	private final Class<T> obj;

	public MainService(Class<T> obj) {
		super();
		this.obj = obj;
		entityManager = PersistenceConfUtil.getEntitymanager();

		checkTransaction();
	}

	public MainService(EntityManager entityManager, Class<T> obj) {
		super();
		this.entityManager = entityManager;
		this.obj = obj;

		checkTransaction();
	}

	public void checkTransaction() {
		if (!this.entityManager.getTransaction().isActive()) {
			this.entityManager.getTransaction().begin();
		}
	}

	public List<T> findAll() throws RuntimeException {
		instances = new LinkedList<>();
		ExecutorService exec = Executors.newWorkStealingPool();
		try {
			exec.submit(new Runnable() {
				@Override
				public void run() {
					instances = findAllProcess();
				}
			});
		} finally {
			exec.shutdown();
			try {
				exec.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return instances;
	}

	private List<T> findAllProcess() throws RuntimeException {
		log.debug(String.format("getting %s instances", obj.getSimpleName()));
		try {
			checkTransaction();
			@SuppressWarnings("unchecked")
			List<T> list = entityManager.createQuery(
					String.format("SELECT e FROM %s e", obj.getSimpleName()))
					.getResultList();
			log.debug("get successful");

			if (list.size() > 0) {
				return list;
			} else {
				throw new RuntimeException("NOT_FOUND");
			}

		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public T findByExample(T arg0) throws RuntimeException {
		instance = null;
		ExecutorService exec = Executors.newWorkStealingPool();
		try {
			exec.submit(new Runnable() {
				@Override
				public void run() {
					instance = findByExampleProcess(arg0);
				}
			});
		} finally {
			exec.shutdown();
			try {
				exec.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	@SuppressWarnings("unchecked")
	private T findByExampleProcess(T arg0) throws RuntimeException {
		Object result = null;
		log.debug(String.format("getting %s instance by example: %s",
				obj.getSimpleName(), arg0.toString()));
		try {
			checkTransaction();
			QueryByExamplePolicy policy = new QueryByExamplePolicy();
			policy.excludeDefaultPrimitiveValues();
			ReadObjectQuery q = new ReadObjectQuery(arg0, policy);
			Query query = JpaHelper.createQuery(q, entityManager);
			result = query.getSingleResult();

			if (result != null) {
				return (T) result;
			} else {
				throw new RuntimeException("NOT_FOUND");
			}

		} catch (RuntimeException re) {
			// TODO Auto-generated catch block
			log.error("get failed", re);
			throw re;
		}
	}

	public T findById(Integer id) throws RuntimeException {
		instance = null;
		ExecutorService exec = Executors.newWorkStealingPool();
		try {
			exec.submit(new Runnable() {
				@Override
				public void run() {
					instance = findByIdProcess(id);
				}
			});
		} finally {
			exec.shutdown();
			try {
				exec.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	private T findByIdProcess(Integer id) throws RuntimeException {
		T instance = null;
		if (id != null) {
			log.debug(String.format("getting %s instance with id: %s",
					obj.getSimpleName(), id));
			try {
				checkTransaction();
				instance = entityManager.find(obj, id);
				log.debug("get successful");
				return instance;
			} catch (RuntimeException re) {
				log.error("get failed", re);
				throw re;
			}
		} else {
			throw new RuntimeException("NOT_FOUND");
		}
	}

	public T merge(T detachedInstance) {
		instance = null;
		ExecutorService exec = Executors.newWorkStealingPool();
		try {
			exec.submit(new Runnable() {
				@Override
				public void run() {
					instance = mergeProcess(detachedInstance);
				}
			});
		} finally {
			exec.shutdown();
			try {
				exec.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return instance;
	}

	private T mergeProcess(T detachedInstance) throws RuntimeException {
		log.debug(String.format("merging %s instance", obj.getSimpleName()));
		try {
			checkTransaction();
			T result = entityManager.merge(detachedInstance);
			entityManager.getTransaction().commit();
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public void persist(T transientInstance) {
		ExecutorService exec = Executors.newWorkStealingPool();
		try {
			exec.submit(new Runnable() {
				@Override
				public void run() {
					persistProcess(transientInstance);
				}
			});
		} finally {
			exec.shutdown();
			try {
				exec.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void persistProcess(T transientInstance) throws RuntimeException {
		try {
			log.debug(String.format("persisting %s instance",
					obj.getSimpleName()));
			checkTransaction();
			entityManager.persist(transientInstance);
			entityManager.getTransaction().commit();
			entityManager.getTransaction().begin();
			log.debug("persist successful, and commit");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void quit() {
		// entityManager.close();
		// factory.close();
		// System.gc();
	}

	public void remove(T persistentInstance) {
		ExecutorService exec = Executors.newWorkStealingPool();
		try {
			exec.submit(new Runnable() {
				@Override
				public void run() {
					removeProcess(persistentInstance);
				}
			});
		} finally {
			exec.shutdown();
			try {
				exec.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void removeProcess(T persistentInstance) throws RuntimeException {
		log.debug(String.format("removing %s instance", obj.getSimpleName()));
		try {
			checkTransaction();
			entityManager.remove(persistentInstance);
			entityManager.getTransaction().commit();
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

}
