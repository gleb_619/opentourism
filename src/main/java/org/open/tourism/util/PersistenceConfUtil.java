package org.open.tourism.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

public class PersistenceConfUtil {

	public static final String PERSISTENCE_UNIT_NAME = "public";

	@PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = PERSISTENCE_UNIT_NAME)
	private volatile static EntityManager entityManager = null;
	private volatile static EntityManagerFactory factory = null;

	public synchronized static EntityManager getEntitymanager() {
		if (entityManager == null) {

			if (factory == null) {
				factory = getFactory();
			}

			entityManager = factory.createEntityManager();
		}

		return entityManager;
	}

	public synchronized static EntityManagerFactory getFactory() {
		if (factory == null) {
			factory = Persistence
					.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}

		return factory;
	}

}
