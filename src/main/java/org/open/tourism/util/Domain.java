package org.open.tourism.util;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.voovee.crm.domain.entity.*;

public enum Domain {

	  ACCOUNT("Account", Account.class)
	, ADDRESS("Address", Address.class)
	, BILL("Bill", Bill.class)
	, BILLINGINFORMATION("BillingInformation", BillingInformation.class)
	, COMMUNICATION("Communication", Communication.class)
	, COMPANY("Company", Company.class)
	, CONTACT("Contact", Contact.class)
	, DEAL("Deal", Deal.class)
	, DEALMEMBER("DealMember", DealMember.class)
	, DICT("Dict", Dict.class)
	, DOCUMENT("Document", Document.class)
	, GOOD("Good", Good.class)
	, PAYMENT("Payment", Payment.class)
	, PRODUCT("Product", Product.class)
	, RIGHT("Right", Right.class)
	, ROLE("Role", Role.class)
	, STRUCTURE("Structure", Structure.class)
	, TAG("Tag", Tag.class)
	, TEAMMEMBER("TeamMember", TeamMember.class)
	, VUSER("VUser", VUser.class)
	
	, ROOT("", Object.class);

	static public Map<Domain, Object> map;
	static {
		map = new TreeMap<Domain, Object>();
		// map.put(UIMENU, new TabView<UIMenu>(UIMenu.class));
	}

	public static Object getByCode(Domain code) {
		return map.get(code);
	}

	public static List<Domain> has(String regex) {
		List<Domain> domains = new LinkedList<Domain>();
		for (Domain domain : values()) {
			if (domain.code.matches(regex)) {
				domains.add(domain);
			}
		}

		return domains;
	}

	Class<?> clazz;

	String code;

	Domain(String code, Class<?> clazz) {
		this.code = code;
		this.clazz = clazz;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public String getCode() {
		return code;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
