package org.open.tourism.util;

import java.util.Map;
import java.util.TreeMap;

import org.test.ui.view.mydashboard.view.DefaultView;
import org.test.ui.view.mydashboard.view.MyDash;
import org.test.ui.view.mydashboard.view.PageView;
import org.test.ui.view.mydashboard.view.TabSheetView;
import org.test.ui.view.mydashboard.view.TabView;
import org.voovee.crm.domain.entity.*;

import com.vaadin.navigator.View;

public enum ContentType {

	DEFAULTVIEW(DefaultView.class.getSimpleName(), DefaultView.class), MYDASH(
			MyDash.class.getSimpleName(), MyDash.class), PAGEVIEW(
			PageView.class.getSimpleName(), PageView.class), TABSHEETVIEW(
			TabSheetView.class.getSimpleName(), TabSheetView.class), TABVIEW(
			TabView.class.getSimpleName(), TabView.class);

	static public Map<Domain, Object> mapMyDash;
	static public Map<Domain, Object> mapPageView;
	static public Map<Domain, Object> mapTabView;
	static {
		mapPageView = new TreeMap<Domain, Object>();
		mapTabView = new TreeMap<Domain, Object>();
		mapMyDash = new TreeMap<Domain, Object>();

		mapPageView.put(Domain.ACCOUNT, new PageView<Account>(Account.class));
		mapPageView.put(Domain.ADDRESS, new PageView<Address>(Address.class));
		mapPageView.put(Domain.BILL, new PageView<Bill>(Bill.class));
		mapPageView.put(Domain.BILLINGINFORMATION, new PageView<BillingInformation>(BillingInformation.class));
		mapPageView.put(Domain.COMMUNICATION, new PageView<Communication>(Communication.class));
		mapPageView.put(Domain.COMPANY, new PageView<Company>(Company.class));
		mapPageView.put(Domain.CONTACT, new PageView<Contact>(Contact.class));
		mapPageView.put(Domain.DEAL, new PageView<Deal>(Deal.class));
		mapPageView.put(Domain.DEALMEMBER, new PageView<DealMember>(DealMember.class));
		mapPageView.put(Domain.DICT, new PageView<Dict>(Dict.class));
		mapPageView.put(Domain.DOCUMENT, new PageView<Document>(Document.class));
		mapPageView.put(Domain.GOOD, new PageView<Good>(Good.class));
		mapPageView.put(Domain.PAYMENT, new PageView<Payment>(Payment.class));
		mapPageView.put(Domain.PRODUCT, new PageView<Product>(Product.class));
		mapPageView.put(Domain.RIGHT, new PageView<Right>(Right.class));
		mapPageView.put(Domain.ROLE, new PageView<Role>(Role.class));
		mapPageView.put(Domain.STRUCTURE, new PageView<Structure>(Structure.class));
		mapPageView.put(Domain.TAG, new PageView<Tag>(Tag.class));
		mapPageView.put(Domain.TEAMMEMBER, new PageView<TeamMember>(TeamMember.class));
		mapPageView.put(Domain.VUSER, new PageView<VUser>(VUser.class));
		
		mapTabView.put(Domain.ACCOUNT, new TabView<Account>(Account.class));
		mapTabView.put(Domain.ADDRESS, new TabView<Address>(Address.class));
		mapTabView.put(Domain.BILL, new TabView<Bill>(Bill.class));
		mapTabView.put(Domain.BILLINGINFORMATION, new TabView<BillingInformation>(BillingInformation.class));
		mapTabView.put(Domain.COMMUNICATION, new TabView<Communication>(Communication.class));
		mapTabView.put(Domain.COMPANY, new TabView<Company>(Company.class));
		mapTabView.put(Domain.CONTACT, new TabView<Contact>(Contact.class));
		mapTabView.put(Domain.DEAL, new TabView<Deal>(Deal.class));
		mapTabView.put(Domain.DEALMEMBER, new TabView<DealMember>(DealMember.class));
		mapTabView.put(Domain.DICT, new TabView<Dict>(Dict.class));
		mapTabView.put(Domain.DOCUMENT, new TabView<Document>(Document.class));
		mapTabView.put(Domain.GOOD, new TabView<Good>(Good.class));
		mapTabView.put(Domain.PAYMENT, new TabView<Payment>(Payment.class));
		mapTabView.put(Domain.PRODUCT, new TabView<Product>(Product.class));
		mapTabView.put(Domain.RIGHT, new TabView<Right>(Right.class));
		mapTabView.put(Domain.ROLE, new TabView<Role>(Role.class));
		mapTabView.put(Domain.STRUCTURE, new TabView<Structure>(Structure.class));
		mapTabView.put(Domain.TAG, new TabView<Tag>(Tag.class));
		mapTabView.put(Domain.TEAMMEMBER, new TabView<TeamMember>(TeamMember.class));
		mapTabView.put(Domain.VUSER, new TabView<VUser>(VUser.class));

		mapMyDash.put(Domain.ROOT, new MyDash());
	}

	public static boolean contains(String type) {
		for (ContentType contentType : values()) {
			if (contentType.name().equals(type.toUpperCase())) {
				return true;
			}
		}

		return false;
	}

	public static Object getByCode(Domain code, String contentType) {
		Object result = null;

		switch (ContentType.valueOf(contentType)) {
		case TABVIEW:
			result = mapTabView.get(code);
			break;

		case PAGEVIEW:
			result = mapPageView.get(code);
			break;

		case MYDASH:
			result = mapMyDash.get(code);
			break;

		default:
			result = new DefaultView();
			break;
		}

		return result;
	}

	public static View getViewByCode(Domain code, String contentType,
			String... additionalProperties) {
		Object result = null;

		switch (ContentType.valueOf(contentType)) {
		case TABVIEW:
			result = mapTabView.get(code);
			break;

		case PAGEVIEW:
			result = mapPageView.get(code);
			break;

		case TABSHEETVIEW:
			if (additionalProperties.length >= 2) {
				result = new TabSheetView(additionalProperties[0],
						additionalProperties[1]);
			} else {
				result = new DefaultView();
			}
			break;

		case MYDASH:
			result = mapMyDash.get(code);
			break;

		default:
			result = new DefaultView();
			break;
		}

		return (View) result;
	}

	Class<?> clazz;

	String code;

	ContentType(String code, Class<?> clazz) {
		this.code = code;
		this.clazz = clazz;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public String getCode() {
		return code;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
