package org.open.tourism.util;

import javax.servlet.http.Cookie;

import com.vaadin.server.VaadinService;

public class CokieUtil {

	public static Cookie readCokie(String cookieName) {
		Cookie result = null;
		Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();
		for (Cookie cookie : cookies) {
			if (cookieName.equals(cookie.getName())) {
				result = cookie;
				break;
			}
		}

		if (result == null) {
			result = writeCokie(cookieName, "");
		}

		return result;
	}

	public static Cookie writeCokie(String cookieName, String cookieValue) {
		Cookie myCookie = new Cookie(cookieName, cookieValue);
		myCookie.setPath(VaadinService.getCurrentRequest().getContextPath());
		VaadinService.getCurrentResponse().addCookie(myCookie);
		return myCookie;
	}
}
