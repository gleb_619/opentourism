package org.test.ui.view.mydashboard.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.open.tourism.util.CokieUtil;
import org.open.tourism.util.PersistenceConfUtil;
import org.test.ui.view.mydashboard.component.data.TTTable;
import org.test.ui.view.mydashboard.util.FieldUtil;
import org.test.ui.view.mydashboard.view.EditorWindow.EditorSavedEvent;
import org.test.ui.view.mydashboard.view.EditorWindow.EditorSavedListener;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.filter.Filters;
import com.vaadin.data.Container;
import com.vaadin.data.Container.Filter;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.MouseEventDetails.MouseButton;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TableFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class TabView<T> extends VerticalLayout implements View,
		TableFieldFactory, ClickListener {

	class Loader implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			getUI().access(new Runnable() {

				private void process() throws Exception {
					mainContainer = JPAContainerFactory.make(clazz,
							PersistenceConfUtil.PERSISTENCE_UNIT_NAME);

					((TTTable) table).setMainContainer(mainContainer);
				}

				@Override
				public void run() {
					// TODO Auto-generated method stub
					getUI().setPollInterval(-1);

					try {
						process();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});
		}

	}

	private static final long serialVersionUID = 8528281418961700458L;
	private Class<T> clazz;

	private Button deleteButton;
	private Object editableId = null;
	private Button editButton;

	private Component header;
	private List<String> histories;
	private JPAContainer<T> mainContainer;

	private Button newButton;
	private ComboBox searchComboBox;
	private Table table;

	private String textFilter;

	public TabView(Class<T> clazz) {
		super();
		this.clazz = clazz;
	}

	private void buildContent() {
		setSizeFull();

		if (header == null) {
			header = buildToolbar();
		}

		if (table == null) {
			table = buildTable();
		} else {
			((TTTable) table).setMainContainer(mainContainer);
		}

		addComponent(header);
		addComponent(table);
		setExpandRatio(table, 1);
	}

	private Table buildTable() {
		TTTable table = new TTTable(this, clazz, mainContainer, editableId,
				new ValueChangeListener() {

					private static final long serialVersionUID = 6816059453686048825L;

					private void setModificationsEnabled(boolean b) {
						deleteButton.setEnabled(b);
						editButton.setEnabled(b);
					}

					@Override
					public void valueChange(ValueChangeEvent event) {
						setModificationsEnabled(event.getProperty().getValue() != null);
					}

				});

		table.setItemClickListener(new ItemClickListener() {

			private static final long serialVersionUID = -8371860418459927686L;

			@Override
			public void itemClick(ItemClickEvent event) {
				if (event.getButton() == MouseButton.LEFT
						&& event.isDoubleClick()) {
					editableId = event.getItemId();

					addStyleName("editable");
					table.setEditable(true);
				} else if (event.getButton() == MouseButton.LEFT) {
					editableId = null;
					table.setEditable(false);
					removeStyleName("editable");
				}
			}

		});

		return table.init();
	}

	@SuppressWarnings("deprecation")
	private HorizontalLayout buildToolbar() {

		HorizontalLayout toolbar = new HorizontalLayout();
		newButton = new Button("Add", this);
		newButton.addStyleName("default");
		newButton.addStyleName("icon-doc");

		editButton = new Button("Edit", this);
		editButton.addStyleName("icon-edit");
		editButton.setEnabled(false);

		deleteButton = new Button("Delete", this);
		deleteButton.addStyleName("icon-delete");
		deleteButton.setEnabled(false);

		searchComboBox = new ComboBox();

		searchComboBox.setNewItemsAllowed(true);
		searchComboBox.setInputPrompt("Filter");
		searchComboBox.setWidth("100%");
		searchComboBox.setFilteringMode(FilteringMode.CONTAINS);

		searchComboBox.addListener(new ValueChangeListener() {

			private static final long serialVersionUID = -60368889264207356L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				if (event.getProperty().getValue() == null) {
					mainContainer.removeAllContainerFilters();
					mainContainer.applyFilters();
				}

				else {
					histories.add(event.getProperty().getValue().toString());
					textFilter = event.getProperty().getValue().toString();
					writeHistory();
					updateFilters();
				}
			}

		});

		searchComboBox.setImmediate(true);

		toolbar.addComponent(newButton);
		toolbar.addComponent(editButton);
		toolbar.addComponent(deleteButton);
		toolbar.addComponent(searchComboBox);

		toolbar.setWidth("100%");
		toolbar.setExpandRatio(searchComboBox, 1);
		toolbar.setComponentAlignment(searchComboBox, Alignment.TOP_RIGHT);

		return toolbar;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if (event.getButton() == newButton) {
			try {
				createNewItem();
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (event.getButton() == editButton) {
			UI.getCurrent().addWindow(
					new EditorWindow(clazz, table.getItem(table.getValue()))
							.init());
		} else if (event.getButton() == deleteButton) {
			mainContainer.removeItem(table.getValue());

			deleteButton.setEnabled(false);
			editButton.setEnabled(false);
		}

	}

	@Override
	public Field<?> createField(Container container, Object itemId,
			Object propertyId, Component uiContext) {
		// TODO Auto-generated method stub
		boolean editable = editableId.equals(itemId);
		Class<?> clazz2 = container.getType(propertyId);

		Field<?> field = null;
		field = (Field<?>) FieldUtil.getField(container, itemId, propertyId,
				uiContext, clazz2);

		if (!Set.class.equals(container.getItem(itemId)
				.getItemProperty(propertyId).getType())) {
			field.setReadOnly(!editable);
		}

		if (field instanceof TextField) {
			((TextField) field).setNullRepresentation("-");
		}

		field.setStyleName("myField");
		field.setWidth("100%");
		field.setCaption(null);

		field.addValidator(new BeanValidator(clazz, propertyId.toString()));

		return field;
	}

	private void createNewItem() throws InstantiationException,
			IllegalAccessException {
		final BeanItem<T> newItem = new BeanItem<T>(clazz.newInstance());
		EditorWindow editorWindow = new EditorWindow(clazz, newItem).init();

		editorWindow.addListener(new EditorSavedListener() {

			private static final long serialVersionUID = 7934536548035746723L;

			@Override
			public void editorSaved(EditorSavedEvent event) {
				mainContainer.addEntity(newItem.getBean());
			}

		});

		UI.getCurrent().addWindow(editorWindow);
		editorWindow.focus();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		init();
	}

	public TabView<T> init() {
		// removeAllComponents();

		buildContent();
		readHistory();
		loadData();
		return this;
	}

	private void loadData() {
		getUI().setPollInterval(100);
		new Thread(new Loader()).start();
	}

	private void readHistory() {
		if (histories == null) {
			histories = new ArrayList<String>();

			String result = CokieUtil.readCokie("user-history").getValue();

			if (result != "") {
				histories.clear();
				histories.addAll(Arrays.asList(result.split(",")));

				for (String string : histories) {
					searchComboBox.addItem(string);
				}
			}

		}
	}

	private void updateFilters() {
		mainContainer.setApplyFiltersImmediately(false);
		mainContainer.removeAllContainerFilters();

		if (textFilter != null && !textFilter.equals("")) {
			java.lang.reflect.Field[] fields = clazz.getDeclaredFields();

			List<Filter> filters = new ArrayList<Filter>();

			for (java.lang.reflect.Field field : fields) {

				if (String.class.equals(field.getType())) {
					filters.add(Filters.like(field.getName(), textFilter + "%",
							false));
					filters.add(Filters.like(field.getName(), "%" + textFilter,
							false));
				}

			}

			mainContainer.addContainerFilter(Filters.or(filters));
		}

		mainContainer.applyFilters();
	}

	private void writeHistory() {
		String result = "";

		for (String history : histories) {
			result += history + ",";
		}

		if (result.length() > 0) {
			CokieUtil.writeCokie("user-history",
					result.substring(0, result.length() - 1));
		}
	}

}
