package org.test.ui.view.mydashboard.component;

import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.event.LayoutEvents.LayoutClickListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class DashCell extends CssLayout {

	private static final long serialVersionUID = 1L;
	private Window notifications;

	public DashCell() {
	}

	public DashCell(Component child) {
		// HorizontalLayout
		// VerticalLayout

		HorizontalLayout verticalLayout = new HorizontalLayout(child);
		// VerticalLayout verticalLayout = new VerticalLayout(child);
		verticalLayout.setCaption("TEST_HEADER_3");

		if (child.getCaption() != null) {
			verticalLayout.setCaption(child.getCaption());
			if (child instanceof Button == false) {
				child.setCaption("");
			}
		}

		verticalLayout.setExpandRatio(child, 1);

		init(verticalLayout);
	}

	public DashCell(Component... children) {
		VerticalLayout verticalLayout = new VerticalLayout(children);
		verticalLayout.setCaption("TEST_HEADER_3");
		init(verticalLayout);
	}

	private void buildNotifications(ClickEvent event) {
		notifications = new Window("Notifications");
		VerticalLayout l = new VerticalLayout();
		l.setMargin(true);
		l.setSpacing(true);
		notifications.setContent(l);
		notifications.setWidth("300px");
		notifications.addStyleName("notifications");
		notifications.setClosable(false);
		notifications.setResizable(false);
		notifications.setDraggable(false);
		notifications.setPositionX(event.getClientX() - event.getRelativeX());
		notifications.setPositionY(event.getClientY() - event.getRelativeY());
		notifications.setCloseShortcut(KeyCode.ESCAPE, null);
		/*
		 * Label label = new Label( "<hr><b>" + Generator.randomFirstName() +
		 * " " + Generator.randomLastName() +
		 * " created a new report</b><br><span>25 minutes ago</span><br>" +
		 * Generator.randomText(18), ContentMode.HTML); l.addComponent(label);
		 * 
		 * label = new Label("<hr><b>" + Generator.randomFirstName() + " " +
		 * Generator.randomLastName() +
		 * " changed the schedule</b><br><span>2 days ago</span><br>" +
		 * Generator.randomText(10), ContentMode.HTML); l.addComponent(label);
		 */
	}

	public void init(Component content) {
		addStyleName("layout-panel");
		setSizeFull();

		Button configure = new Button();
		configure.addStyleName("configure");
		configure.addStyleName("icon-cog");
		configure.addStyleName("icon-only");
		configure.addStyleName("borderless");
		configure.setDescription("Configure");
		configure.addStyleName("small");
		configure.addClickListener(new ClickListener() {

			private static final long serialVersionUID = -2029316544128712911L;

			@Override
			public void buttonClick(ClickEvent event) {

				try {
					// ((DashboardUI) getUI()).clearDashboardButtonBadge();
					event.getButton().removeStyleName("unread");
					event.getButton().setDescription("Notifications");
				} catch (Exception e) {
					// TODO Auto-generated catch block
				}

				if (notifications != null && notifications.getUI() != null)
					notifications.close();
				else {
					buildNotifications(event);
					getUI().addWindow(notifications);
					notifications.focus();
					((CssLayout) getUI().getContent())
							.addLayoutClickListener(new LayoutClickListener() {

								private static final long serialVersionUID = -4883082709012068881L;

								@Override
								public void layoutClick(LayoutClickEvent event) {
									notifications.close();
									((CssLayout) getUI().getContent())
											.removeLayoutClickListener(this);
								}
							});
				}

			}
		});

		addComponent(configure);

		/*
		 * configure.addClickListener(new ClickListener() {
		 * 
		 * @Override public void buttonClick(ClickEvent event) {
		 * Notification.show("Not implemented in this demo _ TEST"); } });
		 */

		addComponent(content);

		setSizeFull();
		// setMargin(new MarginInfo(true, true, false, true));
		// setSpacing(true);

		// return panel;
	}

}
