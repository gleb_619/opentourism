package org.test.ui.view.mydashboard.component;

import java.io.Serializable;

public interface DefaultVooveeComponent<VooveeComponent> extends Serializable{

	public VooveeComponent init();
	
}
