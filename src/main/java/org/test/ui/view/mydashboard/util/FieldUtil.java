package org.test.ui.view.mydashboard.util;

import java.util.Set;

import com.vaadin.ui.Component;
import com.vaadin.ui.DateField;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.TextField;
//import org.test.ui.view.selector.*;
//import org.test.ui.view.selector.data.MainSelector;
import com.vaadin.data.Container;
import com.vaadin.data.Item;

public class FieldUtil {

	public static Object getField(Container container, Object itemId,
			Object propertyId, Component uiContext, Class<?> clazz) {

		Field<?> field = null;

		// List<Domain> domains =
		// Arrays.asList(Domain.values());

		if (Set.class.equals(container.getItem(itemId)
				.getItemProperty(propertyId).getType())) {
			field = new TextField();
			field.setReadOnly(true);
			field.setStyleName("myFieldReadOnly");
		}
		/*
		 * else
		 * if(domains.contains(container.getItem(itemId).getItemProperty(propertyId
		 * ).getType())){ field = new MainSelector<clazz::getTypeName>(clazz); }
		 */else {
			field = DefaultFieldFactory.get().createField(container, itemId,
					propertyId, uiContext);
		}

		return field;
	}

	@SuppressWarnings("unchecked")
	public static Object getField(Item item, Object propertyId,
			Component uiContext, Class<?> clazz) {
		Field<?> field = null;

		if (Set.class.equals(item.getItemProperty(propertyId).getType())) {
			field = new TextField();
			field.setCaption(propertyId.toString());
			field.setReadOnly(true);
			field.setStyleName("myFieldReadOnly");
		} else if (item.getItemProperty(propertyId).getType().getPackage()
				.toString().contains("org.open.tourism.domain.entity")) {
			/*
			 * if (LocationTest.class.equals(item.getItemProperty(propertyId)
			 * .getType())) { field = new
			 * MainSelector<LocationTest>(LocationTest.class); } else {
			 * 
			 * field = new MainSelector<DefaultEntity>(item.getItemProperty(
			 * propertyId).getType()); }
			 */
		} else {
			field = DefaultFieldFactory.get().createField(item, propertyId,
					uiContext);
		}

		if (Integer.class.equals(item.getItemProperty(propertyId).getType())) {
			((TextField) field).setConverter(Integer.class);
		}

		if (field instanceof DateField) {
			((DateField) field).setDateFormat("dd-MM-yyyy");
		}

		return field;
	}

	public static Field<?> getField(String string) {
		// TODO Auto-generated method stub
		return null;
	}

}
