package org.test.ui.view.mydashboard.component;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

public class LoginLayout extends VerticalLayout {

	private static final long serialVersionUID = -7369096505231079069L;

	private Component loginPanel;

	public LoginLayout() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoginLayout(Component loginPanel) {
		super();
		this.loginPanel = loginPanel;
	}

	public Component getLoginPanel() {
		return loginPanel;
	}

	public LoginLayout init() {
		addStyleName("login-layout");
		setSizeFull();

		if (loginPanel != null) {
			addComponent(loginPanel);
			setComponentAlignment(loginPanel, Alignment.MIDDLE_CENTER);
		}

		return this;
	}

	public void setLoginPanel(Component loginPanel) {
		this.loginPanel = loginPanel;
	}

}
