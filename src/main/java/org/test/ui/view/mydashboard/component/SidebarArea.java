package org.test.ui.view.mydashboard.component;

import javax.servlet.http.Cookie;

import org.open.tourism.util.CokieUtil;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.VerticalLayout;

public class SidebarArea extends VerticalLayout {

	private static final long serialVersionUID = -2812027217372032629L;

	private Boolean collapse = false;
	private Component logoLayout;
	private Component menuLayout;
	private Component userLayout;

	public SidebarArea() {
		super();

	}

	public SidebarArea(Component menuLayout) {
		super();
		this.menuLayout = menuLayout;
	}

	public SidebarArea(Component logoLayout, Component menuLayout,
			Component userLayout) {
		super();
		this.logoLayout = logoLayout;
		this.menuLayout = menuLayout;
		this.userLayout = userLayout;
	}

	public void collapseIt() {
		if (collapse) {
			show();

		} else {
			hide();

		}

		collapse = collapse ? false : true;
		CokieUtil.writeCokie("user-collapsed-panel", collapse.toString());

	}

	public Component getLogoLayout() {
		return logoLayout;
	}

	public Component getMenuLayout() {
		return menuLayout;
	}

	public Component getUserLayout() {
		return userLayout;
	}

	private void hide() {
		removeAllComponents();

		removeStyleName("sidebar");
		addStyleName("collapsed-panel");

		MenuItem button = new MenuItem("").init();
		button.setIcon(new ThemeResource("img/show_32.png"));
		button.addStyleName("show-button");

		button.addClickListener((e) -> {
			collapseIt();
		});

		MenuItem button2 = new MenuItem("TEST2").init();
		button2.setIcon(new ThemeResource("img/show_32.png"));

		button2.addClickListener((e) -> {
			collapseIt();
		});

		CssLayout collapseLayout = new CssLayout();

		Image image = new Image();
		image.setIcon(new ThemeResource("img/show_32.png"));

		image.addClickListener((e) -> {
			collapseIt();
			System.out.println("click");
		});

		collapseLayout.addComponent(button);
		button2.setSizeFull();

		addComponent(collapseLayout);
		setExpandRatio(collapseLayout, 1);
		setComponentAlignment(collapseLayout, Alignment.MIDDLE_CENTER);
		collapseLayout.addStyleName("collapse-panel");
	}

	public SidebarArea init() {
		addStyleName("sidebar");
		setWidth(null);
		setHeight("100%");

		Cookie cookie = CokieUtil.readCokie("user-collapsed-panel");

		if (logoLayout != null) {
			addComponent(logoLayout);
		}
		if (menuLayout != null) {
			addComponent(menuLayout);
			setExpandRatio(menuLayout, 1);
		}
		if (userLayout != null) {
			addComponent(userLayout);
		}

		if (!"".equals(cookie.getValue())) {
			collapse = Boolean.valueOf(cookie.getValue());
			if (collapse) {
				hide();
			} else {
				show();
			}
		}

		return this;
	}

	public Boolean isCollapse() {
		return collapse;
	}

	public void setCollapse(Boolean collapse) {
		this.collapse = collapse;
	}

	public void setLogoLayout(Component logoLayout) {
		this.logoLayout = logoLayout;
	}

	public void setMenuLayout(Component menuLayout) {
		this.menuLayout = menuLayout;
	}

	public void setUserLayout(Component userLayout) {
		this.userLayout = userLayout;
	}

	private void show() {
		removeAllComponents();

		if (logoLayout != null) {
			addComponent(logoLayout);
		}
		if (menuLayout != null) {
			addComponent(menuLayout);
			setExpandRatio(menuLayout, 1);
		}
		if (userLayout != null) {
			addComponent(userLayout);
		}

		// setWidth(null);
		removeStyleName("collapsed-panel");
		addStyleName("sidebar");
	}

}
