package org.test.ui.view.mydashboard.view;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class ErrorView extends VerticalLayout implements View {

	private static final long serialVersionUID = -5317509957918083346L;

	public ErrorView() {
		addComponent(new Label(
				"Oops. The view you tried to navigate to doesn't exist."));
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

	}

}
