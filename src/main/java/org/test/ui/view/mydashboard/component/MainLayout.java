package org.test.ui.view.mydashboard.component;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;

public class MainLayout extends HorizontalLayout {

	private static final long serialVersionUID = 4139918030809015891L;

	private Component contentArea;
	private Component sidebarArea;

	public MainLayout() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MainLayout(Component sidebarArea, Component contentArea) {
		super();
		this.sidebarArea = sidebarArea;
		this.contentArea = contentArea;

	}

	public MainLayout init() {
		setSizeFull();
		addStyleName("main-view");

		if (sidebarArea != null) {
			addComponent(sidebarArea);
		}

		if (contentArea != null) {
			addComponent(contentArea);
			setExpandRatio(contentArea, 1);
		}

		return this;
	}

}
