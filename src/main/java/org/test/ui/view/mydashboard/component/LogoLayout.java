package org.test.ui.view.mydashboard.component;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Image;
import com.vaadin.ui.VerticalLayout;

public class LogoLayout extends VerticalLayout {

	private static final long serialVersionUID = -3575476489629746470L;

	private String icon;
	private Component logoArea;

	public LogoLayout() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LogoLayout(Component logoArea) {
		super();
		this.logoArea = logoArea;
	}

	public LogoLayout(Component logoArea, String icon) {
		super();
		this.logoArea = logoArea;
		this.icon = icon;
	}

	public LogoLayout init() {
		if (icon != null) {
			Image image = new Image(null, new ThemeResource(icon));
			addComponent(image);
			setComponentAlignment(image, Alignment.MIDDLE_CENTER);

		}
		if (logoArea != null) {
			addComponent(logoArea);
			setExpandRatio(logoArea, 1);
		}

		return this;
	}

}
