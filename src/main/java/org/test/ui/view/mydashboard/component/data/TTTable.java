package org.test.ui.view.mydashboard.component.data;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Container;
import com.vaadin.event.Action;
import com.vaadin.event.Action.Handler;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.event.ShortcutAction;
//import com.vaadin.server.ErrorEvent;
//import com.vaadin.server.ErrorHandler;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;
import com.vaadin.ui.TableFieldFactory;

public class TTTable extends Table {

	private static final long serialVersionUID = -630740282478605996L;

	private Class<?> clazz;
	private Object editableId;
	private ItemClickListener itemClickListener;
	private List<String> list;
	private JPAContainer<?> mainContainer;
	private TableFieldFactory tableFieldFactory;

	private ValueChangeListener valueChangeListener;

	public TTTable() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TTTable(String caption) {
		super(caption);
		// TODO Auto-generated constructor stub
	}

	public TTTable(String caption, Container dataSource) {
		super(caption, dataSource);
		// TODO Auto-generated constructor stub
	}

	public TTTable(TableFieldFactory tableFieldFactory, Class<?> clazz,
			JPAContainer<?> mainContainer, Object editableId,
			com.vaadin.data.Property.ValueChangeListener valueChangeListener) {
		super();
		this.tableFieldFactory = tableFieldFactory;
		this.clazz = clazz;
		this.mainContainer = mainContainer;
		this.editableId = editableId;
		this.valueChangeListener = valueChangeListener;
	}

	public TTTable(TableFieldFactory tableFieldFactory, Class<?> clazz,
			JPAContainer<?> mainContainer, Object editableId,
			com.vaadin.data.Property.ValueChangeListener valueChangeListener,
			ItemClickListener itemClickListener) {
		super();
		this.tableFieldFactory = tableFieldFactory;
		this.clazz = clazz;
		this.mainContainer = mainContainer;
		this.editableId = editableId;
		this.valueChangeListener = valueChangeListener;
		this.itemClickListener = itemClickListener;
	}

	public TTTable(TableFieldFactory tableFieldFactory, Class<?> clazz,
			Object editableId,
			com.vaadin.data.Property.ValueChangeListener valueChangeListener) {
		super();
		this.tableFieldFactory = tableFieldFactory;
		this.clazz = clazz;
		this.editableId = editableId;
		this.valueChangeListener = valueChangeListener;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public Object getEditableId() {
		return editableId;
	}

	public ItemClickListener getItemClickListener() {
		return itemClickListener;
	}

	public JPAContainer<?> getMainContainer() {
		return mainContainer;
	}

	@Override
	public TableFieldFactory getTableFieldFactory() {
		return tableFieldFactory;
	}

	public ValueChangeListener getValueChangeListener() {
		return valueChangeListener;
	}

	@SuppressWarnings("deprecation")
	public TTTable init() {
		setSelectable(true);
		// setMultiSelect(true);
		setImmediate(true);

		setPageLength(25);
		addStyleName("borderless");
		setSizeFull();
		setRowHeaderMode(RowHeaderMode.INDEX);
		setColumnCollapsingAllowed(true);
		setColumnReorderingAllowed(true);

		setErrorHandler(event -> {
			// TODO Auto-generated method stub
			System.out.println(this.getClass().getName() + "ERROR: "
					+ event.getThrowable().getLocalizedMessage());
		});

		setTableFieldFactory(tableFieldFactory);

		if (list == null) {
			java.lang.reflect.Field[] fields = clazz.getDeclaredFields();
			list = new LinkedList<String>();

			for (java.lang.reflect.Field field : fields) {
				if (!"serialVersionUID".equals(field.getName())) {
					list.add(field.getName());
				}
			}

			Collections.sort(list);
		}

		if (mainContainer != null) {
			setContainerDataSource(mainContainer, list);
		}

		/*
		 * addItemClickListener(new ItemClickListener() {
		 * 
		 * private static final long serialVersionUID = -8371860418459927686L;
		 * 
		 * @Override public void itemClick(ItemClickEvent event) { if
		 * (event.getButton() == MouseButton.LEFT && event.isDoubleClick()) {
		 * editableId = event.getItemId();
		 * 
		 * addStyleName("editable"); setEditable(true); } else if
		 * (event.getButton() == MouseButton.LEFT) { editableId = null;
		 * setEditable(false); removeStyleName("editable"); } } });
		 */

		if (itemClickListener != null) {
			addItemClickListener(itemClickListener);
		}

		addActionHandler(new Handler() {

			private static final long serialVersionUID = 477760698998252115L;

			private Action delete = new Action("Delete");
			private Action edit = new Action("Edit");
			private Action editable = new ShortcutAction("Inline edit",
					ShortcutAction.KeyCode.C,
					new int[] { ShortcutAction.ModifierKey.ALT });
			private Action report = new Action("Report");

			@Override
			public Action[] getActions(Object target, Object sender) {
				return new Action[] { edit, delete, report, editable };
			}

			@Override
			public void handleAction(Action action, Object sender, Object target) {
				if (action == editable) {
					if (editableId != null) {
						editableId = null;
						setEditable(false);
						removeStyleName("editable");
					} else {
						editableId = getValue();

						if (editableId instanceof Collection<?>) {
							editableId = ((Collection<?>) editableId)
									.iterator().next();
						}

						addStyleName("editable");
						setEditable(true);
					}
				} else {
					Notification.show("Not implemented in this demo");
				}
			}
		});

		addListener(valueChangeListener);

		addListener(new ItemClickListener() {

			private static final long serialVersionUID = -9153862341073066928L;

			@Override
			public void itemClick(ItemClickEvent event) {
				if (event.isDoubleClick()) {
					select(event.getItemId());
				}
			}
		});

		return this;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	public void setEditableId(Object editableId) {
		this.editableId = editableId;
	}

	public void setItemClickListener(ItemClickListener itemClickListener) {
		this.itemClickListener = itemClickListener;
		addItemClickListener(itemClickListener);
	}

	public void setMainContainer(JPAContainer<?> mainContainer) {
		this.mainContainer = mainContainer;
		setContainerDataSource(mainContainer, list);
	}

	@Override
	public void setTableFieldFactory(TableFieldFactory tableFieldFactory) {
		this.tableFieldFactory = tableFieldFactory;
	}

	public void setValueChangeListener(ValueChangeListener valueChangeListener) {
		this.valueChangeListener = valueChangeListener;
	}

}
