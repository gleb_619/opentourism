/**
 * DISCLAIMER
 * 
 * The quality of the code is such that you should not copy any of it as best
 * practice how to build Vaadin applications.
 * 
 * @author jouni@vaadin.com
 * 
 */

package org.test.ui.view.mydashboard.ui.factory;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.DefaultConverterFactory;

@SuppressWarnings("serial")
public class DefaultDomainFactory extends DefaultConverterFactory {
	@SuppressWarnings("unchecked")
	@Override
	protected <PRESENTATION, MODEL> Converter<PRESENTATION, MODEL> findConverter(
			Class<PRESENTATION> presentationType, Class<MODEL> modelType) {

		if (presentationType == String.class && modelType == Set.class) {
			return (Converter<PRESENTATION, MODEL>) StringToSetConvertor();
		}

		if (presentationType == Set.class && modelType == String.class) {
			return (Converter<PRESENTATION, MODEL>) SetToStringConvertor();
		}
		/*
		 * Class<?> clazz = presentationType.getSuperclass(); if
		 * (clazz.equals(DefaultEntity.class) && modelType == String.class) {
		 * return (Converter<PRESENTATION, MODEL>)
		 * DefaultEntityToStringConvertor(); }
		 * 
		 * clazz = modelType.getSuperclass(); if ( presentationType ==
		 * String.class && clazz .equals( DefaultEntity.class)) { return
		 * (Converter<PRESENTATION, MODEL>) StringToDefaultEntityConvertor(); }
		 */
		/*
		 * if(presentationType == DefaultEntity.class){ return
		 * (Converter<PRESENTATION, MODEL>) DefaultEntityToStringConvertor(); }
		 * 
		 * if(modelType == DefaultEntity.class){ return (Converter<PRESENTATION,
		 * MODEL>) StringToDefaultEntityConvertor(); }
		 */
		/*
		 * System.out.println("presentationType :" + presentationType);
		 * System.out.println("modelType        :" + modelType);
		 * System.out.println("-------------");
		 */
		return super.findConverter(presentationType, modelType);
	}

	private Converter<Set, String> SetToStringConvertor() {
		return (new Converter<Set, String>() {

			@Override
			public String convertToModel(Set value,
					Class<? extends String> targetType, Locale locale)
					throws com.vaadin.data.util.converter.Converter.ConversionException {
				// TODO Auto-generated method stub
				return "...";
			}

			@Override
			public Set<Object> convertToPresentation(String value,
					Class<? extends Set> targetType, Locale locale)
					throws com.vaadin.data.util.converter.Converter.ConversionException {
				// TODO Auto-generated method stub
				return new HashSet<>();
			}

			@Override
			public Class<String> getModelType() {
				// TODO Auto-generated method stub
				return String.class;
			}

			@Override
			public Class<Set> getPresentationType() {
				// TODO Auto-generated method stub
				return Set.class;
			}
		});
	}

	/*
	 * private Converter<String, DefaultEntity> StringToDefaultEntityConvertor()
	 * { return (new Converter<String, DefaultEntity>() {
	 * 
	 * @Override public DefaultEntity convertToModel(String value, Class<?
	 * extends DefaultEntity> targetType, Locale locale) throws
	 * com.vaadin.data.util.converter.Converter.ConversionException { // TODO
	 * Auto-generated method stub return new DefaultEntity(value.toString()); }
	 * 
	 * @Override public String convertToPresentation(DefaultEntity value,
	 * Class<? extends String> targetType, Locale locale) throws
	 * com.vaadin.data.util.converter.Converter.ConversionException { // TODO
	 * Auto-generated method stub return value.getName() == null ? "" :
	 * value.getName(); }
	 * 
	 * @Override public Class<DefaultEntity> getModelType() { // TODO
	 * Auto-generated method stub return DefaultEntity.class; }
	 * 
	 * @Override public Class<String> getPresentationType() { // TODO
	 * Auto-generated method stub return String.class; } }); }
	 * 
	 * private Converter<DefaultEntity, String> DefaultEntityToStringConvertor()
	 * { return (new Converter<DefaultEntity, String>() {
	 * 
	 * @Override public String convertToModel(DefaultEntity value, Class<?
	 * extends String> targetType, Locale locale) throws
	 * com.vaadin.data.util.converter.Converter.ConversionException { // TODO
	 * Auto-generated method stub return value.getName() == null ? "" :
	 * value.getName(); }
	 * 
	 * @Override public DefaultEntity convertToPresentation(String value,
	 * Class<? extends DefaultEntity> targetType, Locale locale) throws
	 * com.vaadin.data.util.converter.Converter.ConversionException { // TODO
	 * Auto-generated method stub return new DefaultEntity(value.toString()); }
	 * 
	 * @Override public Class<String> getModelType() { // TODO Auto-generated
	 * method stub return String.class; }
	 * 
	 * @Override public Class<DefaultEntity> getPresentationType() { // TODO
	 * Auto-generated method stub return DefaultEntity.class; } }); }
	 */
	private Converter<String, Set> StringToSetConvertor() {
		return (new Converter<String, Set>() {

			@Override
			public Set<Object> convertToModel(String value,
					Class<? extends Set> targetType, Locale locale)
					throws com.vaadin.data.util.converter.Converter.ConversionException {
				// TODO Auto-generated method stub
				return new HashSet<>();
			}

			@Override
			public String convertToPresentation(Set value,
					Class<? extends String> targetType, Locale locale)
					throws com.vaadin.data.util.converter.Converter.ConversionException {
				// TODO Auto-generated method stub
				String result = "";

				if (value.size() > 0) {
					result = "[...]";
				}

				return result;
			}

			@Override
			public Class<Set> getModelType() {
				// TODO Auto-generated method stub
				return Set.class;
			}

			@Override
			public Class<String> getPresentationType() {
				// TODO Auto-generated method stub
				return String.class;
			}

		});
	}

}
