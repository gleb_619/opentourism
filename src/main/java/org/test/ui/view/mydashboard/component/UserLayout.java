package org.test.ui.view.mydashboard.component;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.VerticalLayout;

public class UserLayout extends VerticalLayout {

	private static final long serialVersionUID = -2947950133201749414L;

	private String firstname;
	private String lastname;
	private String profileImage = "img/defaultuser.png";
	private MenuBar settings;

	public UserLayout() {
		super();
	}

	public UserLayout(String firstname, String lastname) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
	}

	public UserLayout(String profileImage, String firstname, String lastname,
			MenuBar settings) {
		super();
		this.profileImage = profileImage;
		this.firstname = firstname;
		this.lastname = lastname;
		this.settings = settings;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public MenuBar getSettings() {
		return settings;
	}

	public UserLayout init() {
		setSizeUndefined();
		addStyleName("user");
		Image profilePic = new Image(null, new ThemeResource(profileImage));

		if (profilePic.getWidth() > 23 && profilePic.getWidth() < 33) {
			addComponent(profilePic);
		} else {
			profilePic = new Image(null, new ThemeResource(
					"img/defaultuser.png"));
			addComponent(profilePic);
		}

		if (firstname != null && lastname != null) {
			Label userName = new Label(lastname + " " + firstname);
			userName.setSizeUndefined();
			addComponent(userName);
		}

		settings = new MenuBar();
		MenuItem settingsMenu = settings.addItem("", null);
		settingsMenu.setStyleName("icon-cog");
		settingsMenu.addItem("Config", null);
		addComponent(settings);

		Button exit = new NativeButton("Exit");
		exit.addStyleName("icon-cancel");
		addComponent(exit);

		return this;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public void setSettings(MenuBar settings) {
		this.settings = settings;
	}

}
