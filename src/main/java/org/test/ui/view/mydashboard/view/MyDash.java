package org.test.ui.view.mydashboard.view;

import org.test.ui.view.mydashboard.component.DashCell;
import org.test.ui.view.mydashboard.component.DashRow;
import org.test.ui.view.mydashboard.component.LoginLayout;
import org.test.ui.view.mydashboard.component.MenuLayout;
import org.test.ui.view.mydashboard.component.SidebarArea;
import org.test.ui.view.mydashboard.component.login.LoginPanel;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.RowHeaderMode;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("deprecation")
public class MyDash extends VerticalLayout implements View {

	private static final long serialVersionUID = 1409034747985729639L;

	public MyDash() {
		super();

	}

	private VerticalLayout buildContent() {
		VerticalLayout content = new VerticalLayout();

		content.addStyleName("dashboard-view");
//		content.setHeight("600%");
		content.setSizeUndefined();
		content.setWidth("100%");

		for (int i = 0; i < 10; i++) {

			DashCell[] children = null;

			DashRow dashRow = null;

			if (i == 0) {
				final Label title = new Label("My Dash");
				title.setSizeUndefined();
				title.addStyleName("h1");
				dashRow = new DashRow(true, false, title);
				// dashRow.addComponent(title);
				dashRow.setComponentAlignment(title, Alignment.MIDDLE_LEFT);
				dashRow.setExpandRatio(title, 1);

				HorizontalLayout horizontalLayout = buildToolbarButton(title);
				dashRow.addComponent(horizontalLayout);
				dashRow.setComponentAlignment(horizontalLayout,
						Alignment.MIDDLE_RIGHT);

				content.addComponent(dashRow);
				// content.setExpandRatio(dashRow, 0.1f);
			} else if (i == 1) {
				Form form = new Form();
				form.setCaption("TEST FORM");

				children = new DashCell[1];
				children[0] = new DashCell(form);
				// children[0].init(form);

				dashRow = new DashRow(children);
				content.addComponent(dashRow);
				content.setExpandRatio(dashRow, 1.5f);
			} else if (i == 2) {

				MenuLayout menuLayout = new MenuLayout().init();
				SidebarArea sidebarArea = new SidebarArea(menuLayout);

				children = new DashCell[1];
				children[0] = new DashCell(sidebarArea);

				dashRow = new DashRow(children);
				content.addComponent(dashRow);
				content.setExpandRatio(dashRow, 1.5f);

			} else if (i == 3) {

				try {
					LoginPanel loginPanel = new LoginPanel("TEST").init();
					LoginLayout loginLayout = new LoginLayout(loginPanel)
							.init();

					children = new DashCell[1];
					children[0] = new DashCell(loginLayout);

					dashRow = new DashRow(children);
					content.addComponent(dashRow);
					content.setExpandRatio(dashRow, 1.5f);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else if (i % 2 == 0) {

				try {
					children = new DashCell[1];
					children[0] = new DashCell(new Button("TEST1" + i));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				dashRow = new DashRow(children);
				content.addComponent(dashRow);
				content.setExpandRatio(dashRow, 0.9f);

			} else {

				try {
					children = new DashCell[2];
					children[0] = new DashCell(new Button("TEST_" + i));
					children[1] = new DashCell(new Button("TEST_" + i + 1));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				dashRow = new DashRow(children);
				content.addComponent(dashRow);
				content.setExpandRatio(dashRow, 1.0f);
			}

		}

		Table table = new Table();
		table.setPageLength(0);
		table.addStyleName("plain");
		table.addStyleName("borderless");
		table.setRowHeaderMode(RowHeaderMode.INDEX);
		// table.setSizeFull();

		DashCell cell = new DashCell(table);

		DashRow dashRow = new DashRow(cell);

		content.addComponent(dashRow);
		content.setExpandRatio(dashRow, 2.0f);

		return content;
	}

	private HorizontalLayout buildToolbarButton(Label title) {
		HorizontalLayout horizontalLayout = new HorizontalLayout();
		Button notify = new Button("2");

		notify.setDescription("Notifications (2 unread)");
		// notify.addStyleName("borderless");
		notify.addStyleName("notifications");
		notify.addStyleName("unread");
		notify.addStyleName("icon-only");
		notify.addStyleName("icon-bell");
		notify.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 6510254074520973411L;

			@Override
			public void buttonClick(ClickEvent event) {
				// ((DashboardUI) getUI()).clearDashboardButtonBadge();
				event.getButton().removeStyleName("unread");
				event.getButton().setDescription("Notifications");
				/*
				 * if (notifications != null && notifications.getUI() != null)
				 * notifications.close(); else { buildNotifications(event);
				 * getUI().addWindow(notifications); notifications.focus();
				 * ((CssLayout) getUI().getContent())
				 * .addLayoutClickListener(new LayoutClickListener() {
				 * 
				 * @Override public void layoutClick(LayoutClickEvent event) {
				 * notifications.close(); ((CssLayout) getUI().getContent())
				 * .removeLayoutClickListener(this); } }); }
				 */

			}
		});
		horizontalLayout.addComponent(notify);
		horizontalLayout.setComponentAlignment(notify, Alignment.MIDDLE_LEFT);

		Button edit = new Button();
		edit.addStyleName("icon-edit");
		edit.addStyleName("icon-only");
		horizontalLayout.addComponent(edit);
		edit.setDescription("Edit Dashboard");
		edit.addClickListener(new ClickListener() {

			private static final long serialVersionUID = 3313781085417749405L;

			@Override
			public void buttonClick(ClickEvent event) {
				final Window w = new Window("Edit Dashboard");

				w.setModal(true);
				w.setClosable(false);
				w.setResizable(false);
				w.addStyleName("edit-dashboard");

				getUI().addWindow(w);

				w.setContent(new VerticalLayout() {

					private static final long serialVersionUID = -4117939867211952476L;

					TextField name = new TextField("Dashboard Name");
					{
						addComponent(new FormLayout() {

							private static final long serialVersionUID = -2129169028312153089L;

							{
								setSizeUndefined();
								setMargin(true);
								name.setValue(title.getValue());
								addComponent(name);
								name.focus();
								name.selectAll();
							}
						});

						addComponent(new HorizontalLayout() {

							private static final long serialVersionUID = -8688991589167828918L;

							{
								setMargin(true);
								setSpacing(true);
								addStyleName("footer");
								setWidth("100%");

								Button cancel = new Button("Cancel");
								cancel.addClickListener(new ClickListener() {

									private static final long serialVersionUID = 6032725235428452290L;

									@Override
									public void buttonClick(ClickEvent event) {
										w.close();
									}
								});
								cancel.setClickShortcut(KeyCode.ESCAPE, null);
								addComponent(cancel);
								setExpandRatio(cancel, 1);
								setComponentAlignment(cancel,
										Alignment.TOP_RIGHT);

								Button ok = new Button("Save");
								ok.addStyleName("wide");
								ok.addStyleName("default");
								ok.addClickListener(new ClickListener() {

									private static final long serialVersionUID = -8070161711146280077L;

									@Override
									public void buttonClick(ClickEvent event) {
										title.setValue(name.getValue());
										w.close();
									}
								});
								ok.setClickShortcut(KeyCode.ENTER, null);
								addComponent(ok);
							}
						});

					}
				});

			}
		});
		horizontalLayout.setComponentAlignment(edit, Alignment.MIDDLE_LEFT);

		return horizontalLayout;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		init();
	}

	public MyDash init() {
		// removeAllComponents();
		// removeStyleName("v-myverticallayout");

		addStyleName("my-scrollable-panel");
		setSizeFull();
//		addStyleName("v-myverticallayout");

		VerticalLayout content = buildContent();
		addComponent(content);
		content = null;

		return this;
	}


}
