package org.test.ui.view.mydashboard.util;

import org.open.tourism.util.PersistenceConfUtil;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.data.Property;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;

public class MainSelector<T> extends CustomField<T> {

	private static final long serialVersionUID = 1L;
	private ComboBox comboBox = new ComboBox();
	private JPAContainer<T> container;
	private Class<T> obj;

	/*
	 * public MainSelector() { super(); // TODO Auto-generated constructor stub
	 * setCaption("CurrCourse"); container = JPAContainerFactory.make(obj,
	 * JpaAddressbookUI.PERSISTENCE_UNIT);
	 * 
	 * comboBox.setContainerDataSource(container); comboBox.setImmediate(true);
	 * comboBox.setItemCaptionPropertyId( "name" ); }
	 */
	public MainSelector(Class<T> obj) {
		super();
		this.obj = obj;
		setCaption(obj.getSimpleName());
		container = JPAContainerFactory.make(obj,
				PersistenceConfUtil.PERSISTENCE_UNIT_NAME);

		comboBox.setContainerDataSource(container);
		comboBox.setImmediate(true);
		// comboBox.setItemCaptionPropertyId("countryname");
	}

	@Override
	public Class<? extends T> getType() {
		// TODO Auto-generated method stub
		return obj;
	}

	@Override
	protected Component initContent() {
		return comboBox;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void setPropertyDataSource(Property newDataSource) {
		super.setPropertyDataSource(newDataSource);
		try {
			setValueObj((DefaultSelector<T>) newDataSource.getValue());
		} catch (ClassCastException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setValue(T arg0) throws ReadOnlyException,
			Converter.ConversionException {
		super.setValue(arg0);
		try {
			setValueObj((DefaultSelector<T>) arg0);
		} catch (ClassCastException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setValueObj(DefaultSelector<T> arg0) throws ClassCastException {
		this.comboBox.setValue(arg0 != null ? arg0.getObjId() : null);
	}

}
