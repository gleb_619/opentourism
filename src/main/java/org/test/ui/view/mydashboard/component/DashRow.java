package org.test.ui.view.mydashboard.component;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;

public class DashRow extends HorizontalLayout {

	private static final long serialVersionUID = 2533210435132513148L;

	public DashRow() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DashRow(boolean header, boolean footer, Component... children) {
		init(header, footer, children);
	}

	public DashRow(Component... children) {
		init(false, false, children);
	}

	public void init(boolean header, boolean footer, Component... children) {
		if (header) {
			setWidth("100%");
			setSpacing(true);
			addStyleName("toolbar");
			addComponents(children);
		} else if (footer) {
			addComponents(children);
		} else {
			setSizeFull();
			setSpacing(true);
			addComponents(children);
		}

	}

}
