package org.test.ui.view.mydashboard.component;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;

public class LogoArea extends CssLayout {

	private static final long serialVersionUID = -3143829786667523275L;

	private String iconLogo;
	private Label logo;
	private MenuItem menuItem;
	private String name;
	private String slogan;

	public LogoArea() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LogoArea(String name, String slogan) {
		super();
		this.name = name;
		this.slogan = slogan;
	}

	public LogoArea(String iconLogo, String name, String slogan) {
		super();
		this.iconLogo = iconLogo;
		this.name = name;
		this.slogan = slogan;

	}

	public String getIconLogo() {
		return iconLogo;
	}

	public Label getLogo() {
		return logo;
	}

	public MenuItem getMenuItem() {
		return menuItem;
	}

	public String getName() {
		return name;
	}

	public String getSlogan() {
		return slogan;
	}

	public LogoArea init() {
		addStyleName("branding");

		logo = new Label(String.format("<span>%s</span> %s", name, slogan),
				ContentMode.HTML);
		logo.setSizeUndefined();
		addComponent(logo);

		menuItem = new MenuItem().init();
		menuItem.setSizeUndefined();
		menuItem.setWidth("100%");
		menuItem.setDescription("Hide");
		menuItem.setIcon(new ThemeResource("img/hide_32.png"));
		// menuItem.addStyleName("hide-button");
		menuItem.setStyleName("hide-button");
		menuItem.addStyleName("white-label");
		addComponent(menuItem);

		if (iconLogo != null) {
			addComponent(new Image(null, new ThemeResource(iconLogo)));

		}

		return this;
	}

	public void setIconLogo(String iconLogo) {
		this.iconLogo = iconLogo;
	}

	public void setLogo(Label logo) {
		this.logo = logo;
	}

	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSlogan(String slogan) {
		this.slogan = slogan;
	}

}
