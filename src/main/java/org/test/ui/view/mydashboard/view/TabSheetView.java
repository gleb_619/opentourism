package org.test.ui.view.mydashboard.view;

import org.test.ui.view.mydashboard.component.layout.TTDashPanel;
import org.test.ui.view.mydashboard.util.MenuBarUtil;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

public class TabSheetView extends VerticalLayout implements View {

	private static final long serialVersionUID = 1L;
	private CssLayout content = new CssLayout();
	// private Tab tab;
	private String contentType;
	private MenuBar menuBar;
	private String regex;
	private TabSheet tabSheet;

	public TabSheetView(String regex, String contentType) {
		super();
		this.regex = regex;
		this.contentType = contentType;
	}

	@SuppressWarnings("unused")
	private Component buildDash() {
		Integer cells = (int) Math.sqrt(MenuBarUtil.size);
		VerticalLayout root = new VerticalLayout();
		// root.addStyleName("v-myverticallayout");
		GridLayout gridLayout = new GridLayout(cells + 2, cells);

		for (int i = 0; i < MenuBarUtil.size + 10; i++) {
			createDash(gridLayout);
		}

		gridLayout.setSpacing(true);
		gridLayout.setSizeUndefined();
		gridLayout.setWidth("100%");
		root.setSizeFull();
		root.addComponent(gridLayout);

		return root;
	}

	private void createDash(GridLayout gridLayout) {
		TTDashPanel dashPanel = new TTDashPanel(
				"TEST",
				null,
				"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,");
		gridLayout.addComponent(dashPanel);
	}

	@SuppressWarnings("unused")
	private void createDash2(GridLayout gridLayout) {
		Panel panel = new Panel("TEST");
		VerticalLayout verticalLayout = new VerticalLayout();
		VerticalLayout verticalLayout2 = new VerticalLayout();

		VerticalLayout verticalLayout3 = new VerticalLayout();
		// cssLayout.addStyleName("my-dash-item");

		Image image = new Image(null, new ThemeResource("img/doc_64.png"));
		Button button = new Button("click");

		verticalLayout3.addComponent(image);
		verticalLayout3.setComponentAlignment(image, Alignment.MIDDLE_CENTER);
		verticalLayout3.setWidth("150px");
		verticalLayout3.setHeight("170px");
		verticalLayout3.addComponent(button);
		verticalLayout3.setComponentAlignment(button, Alignment.BOTTOM_CENTER);
		// verticalLayout3.setSizeFull();

		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setWidth("100%");
		horizontalLayout.addStyleName("my-label-overflow");
		panel.setContent(verticalLayout);

		verticalLayout.addComponent(new Label("<hr>", ContentMode.HTML));
		horizontalLayout.addComponent(verticalLayout3);

		Label label = new Label("<span>123\n121]n5454]n454788\n55555</span>",
				ContentMode.HTML);
		Label label2 = new Label(
				"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,",
				ContentMode.HTML);
		// label2.addStyleName("my-label-overflow");
		// label.setStyleName("asdasdasd");
		// label.setSizeFull();
		verticalLayout2.addComponent(label);
		verticalLayout2.addComponent(label2);
		verticalLayout2.addComponent(new Label("123"));
		verticalLayout2.addComponent(new Label("123"));
		verticalLayout2.addComponent(new Label("123"));

		horizontalLayout.addComponent(verticalLayout2);
		verticalLayout2.addStyleName("my-dash-block");
		// verticalLayout2.setSizeFull();
		horizontalLayout.setSizeFull();
		horizontalLayout.setExpandRatio(verticalLayout2, 1);
		verticalLayout.addComponent(horizontalLayout);
		verticalLayout.addStyleName("my-dash-panel");

		gridLayout.addComponent(panel);
		gridLayout.setSpacing(true);
		// panel.setSizeFull();
		verticalLayout.setSizeFull();
	}

	/*
	 * private Component buildDashPanel() {
	 * 
	 * }
	 */
	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		init();
	}

	public TabSheetView init() {
		// removeAllComponents();
		// removeStyleName("view-content");

		setSizeFull();
		addStyleName("view-content");

		if (tabSheet == null) {
			tabSheet = new TabSheet();
			// tabSheet.setSizeFull();
			tabSheet.addStyleName("borderless");
		}

		if (menuBar == null) {
			menuBar = new MenuBar();
			menuBar.setImmediate(false);
			menuBar.setWidth("100%");
			// menuBar.setHeight("20px");

			MenuBarUtil.init(regex, contentType, tabSheet, menuBar);
			/*
			 * MenuItem menuItem = menuBar.addItem("", selectedItem -> { Tab tab
			 * = tabSheet.addTab(buildDash()); tab.setCaption("DASH");
			 * tab.setClosable(true); });
			 * 
			 * menuItem.setStyleName("icon-home");
			 */

			content.addComponent(menuBar);
			content.addStyleName("my-menubar");

			addComponent(content);
			addComponent(tabSheet);
			tabSheet.setSizeFull();
			setExpandRatio(tabSheet, 2);
		}

		/*
		 * tab = tabSheet.addTab(buildDash()); tab.setCaption("DASH");
		 * tab.setClosable(true);
		 */
		return this;
	}
}
