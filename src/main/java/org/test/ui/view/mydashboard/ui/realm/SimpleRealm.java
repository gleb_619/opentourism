/**
 * Copyright (c) 2005-2012 https://github.com/zhangkaitao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package org.test.ui.view.mydashboard.ui.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * User: Zhang Kaitao
 * <p>
 * Date: 13-3-12 下午9:05
 * <p>
 * Version: 1.0
 */
public class SimpleRealm extends AuthorizingRealm {

	// private UserService userService;
	// private UserAuthService userAuthService;

	private static final String AND_OPERATOR = " and ";

	private static final Logger log = LoggerFactory.getLogger("es-error");

	private static final String NOT_OPERATOR = "not ";

	private static final String OR_OPERATOR = " or ";

	public SimpleRealm() {
		super();

	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken token) throws AuthenticationException {

		UsernamePasswordToken upToken = (UsernamePasswordToken) token;
		String username = upToken.getUsername().trim();
		String password = "";

		if (upToken.getPassword() != null) {
			password = new String(upToken.getPassword());
		}
		/*
		 * User user = null; try { user = userService.login(username, password);
		 * } catch (UserNotExistsException e) { throw new
		 * UnknownAccountException(e.getMessage(), e); } catch
		 * (UserPasswordNotMatchException e) { throw new
		 * AuthenticationException(e.getMessage(), e); } catch
		 * (UserPasswordRetryLimitExceedException e) { throw new
		 * ExcessiveAttemptsException(e.getMessage(), e); } catch
		 * (UserBlockedException e) { throw new
		 * LockedAccountException(e.getMessage(), e); } catch (Exception e) {
		 * log.error("login error", e); throw new AuthenticationException(new
		 * UserException("user.unknown.error", null)); }
		 */

		System.out.println("username:" + username);
		System.out.println("password:" + password);

		SimpleAuthenticationInfo info = null;

		if ("12345".equals(username) && "12345".equals(password)) {
			info = new SimpleAuthenticationInfo("12345", "12345".toCharArray(),
					getName());
		} else {
			// throw new AuthenticationException(new
			// Exception("user.unknown.error", null));
		}

		return info;
	}

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(
			PrincipalCollection principals) {
		String username = (String) principals.getPrimaryPrincipal();
		for (Object object : principals.asList()) {
			System.out.println(object);
		}

		// User user = userService.findByUsername(username);

		SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
		// authorizationInfo.s
		// authorizationInfo.setRoles(userAuthService.findStringRoles(user));
		// authorizationInfo.setStringPermissions(userAuthService.findStringPermissions(user));

		return authorizationInfo;
	}

	/**
	 * 支持or and not 关键词 不支持and or混用
	 *
	 * @param principals
	 * @param permission
	 * @return
	 */
	@Override
	public boolean isPermitted(PrincipalCollection principals, String permission) {
		if (permission.contains(OR_OPERATOR)) {
			String[] permissions = permission.split(OR_OPERATOR);
			for (String orPermission : permissions) {
				if (isPermittedWithNotOperator(principals, orPermission)) {
					return true;
				}
			}
			return false;
		} else if (permission.contains(AND_OPERATOR)) {
			String[] permissions = permission.split(AND_OPERATOR);
			for (String orPermission : permissions) {
				if (!isPermittedWithNotOperator(principals, orPermission)) {
					return false;
				}
			}
			return true;
		} else {
			return isPermittedWithNotOperator(principals, permission);
		}
	}

	private boolean isPermittedWithNotOperator(PrincipalCollection principals,
			String permission) {
		if (permission.startsWith(NOT_OPERATOR)) {
			return !super.isPermitted(principals,
					permission.substring(NOT_OPERATOR.length()));
		} else {
			return super.isPermitted(principals, permission);
		}
	}

}
