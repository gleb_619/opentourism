package org.test.ui.view.mydashboard.component.login;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class LoginPanel extends CssLayout {

	private static final long serialVersionUID = 6363572354149337159L;

	private HorizontalLayout labels;
	private String title;

	public LoginPanel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoginPanel(String title) {
		super();
		this.title = title;
	}

	public LoginPanel init() {
		if (labels == null) {
			labels = new HorizontalLayout();
		}

		addStyleName("login-panel");
		labels.setWidth("100%");
		labels.setMargin(true);
		labels.addStyleName("labels");
		addComponent(labels);

		Label titleLabel = new Label(title);
		titleLabel.setSizeUndefined();
		titleLabel.addStyleName("h4");
		labels.addComponent(titleLabel);
		labels.setComponentAlignment(titleLabel, Alignment.MIDDLE_LEFT);

		/*
		 * Label titleLabel = new Label(title); titleLabel.setSizeUndefined();
		 * titleLabel.addStyleName("h2"); titleLabel.addStyleName("light");
		 * labels.addComponent(titleLabel);
		 * labels.setComponentAlignment(titleLabel, Alignment.MIDDLE_RIGHT);
		 */

		VerticalLayout fields = new VerticalLayout();
		// HorizontalLayout fields = new HorizontalLayout();

		fields.setSpacing(true);
		fields.setMargin(true);
		fields.addStyleName("fields");

		final TextField username = new TextField("Username");
		username.focus();
		fields.addComponent(username);

		final PasswordField password = new PasswordField("Password");
		fields.addComponent(password);

		addComponent(fields);

		return this;

	}

}
