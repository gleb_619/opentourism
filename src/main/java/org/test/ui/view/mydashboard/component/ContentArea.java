package org.test.ui.view.mydashboard.component;

import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;

public class ContentArea extends CssLayout {

	private static final long serialVersionUID = 2281511771560041103L;

	private Component content;

	public ContentArea() {
		super();
		init();
	}

	public ContentArea(Component content) {
		super();
		this.content = content;
		init();
	}

	public ContentArea(Component... children) {
		super(children);
		// TODO Auto-generated constructor stub
		init();
	}

	public ContentArea init() {
		removeAllComponents();
		removeStyleName("view-content");

		setSizeFull();
		addStyleName("view-content");
		if (content != null) {
			addComponent(content);
		}

		return this;
	}

}
