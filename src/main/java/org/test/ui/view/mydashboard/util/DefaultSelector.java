package org.test.ui.view.mydashboard.util;

public interface DefaultSelector<T> {

	public T getEntity(T arg0);

	public Integer getObjId();

	public void setEntity(T arg0);

}
