package org.test.ui.view.mydashboard.component.layout;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

public class TTDashPanel extends VerticalLayout {

	private static final long serialVersionUID = 3414121256545201026L;

	private ClickListener clickListener;
	private HorizontalLayout contentArea;
	private VerticalLayout controlArea;
	private VerticalLayout descriptionArea;
	private Button detailButton;
	private VerticalLayout footerArea;
	private VerticalLayout headerArea;
	private Label headerLabel;
	private Image panelIcon;

	public TTDashPanel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TTDashPanel(Component... children) {
		super(children);
		// TODO Auto-generated constructor stub
	}

	public TTDashPanel(String caption, String icon, String value) {
		super();
		// TODO Auto-generated constructor stub
		init(caption, icon, value);
	}

	public TTDashPanel(String caption, String icon, String value,
			ClickListener clickListener) {
		super();
		// TODO Auto-generated constructor stub
		this.clickListener = clickListener;
		init(caption, icon, value);
	}

	public TTDashPanel(VerticalLayout headerArea, VerticalLayout controlArea,
			VerticalLayout descriptionArea, HorizontalLayout contentArea,
			VerticalLayout footerArea, Image panelIcon, Label headerLabel,
			Button detailButton, ClickListener clickListener) {
		super();
		this.headerArea = headerArea;
		this.controlArea = controlArea;
		this.descriptionArea = descriptionArea;
		this.contentArea = contentArea;
		this.footerArea = footerArea;
		this.panelIcon = panelIcon;
		this.headerLabel = headerLabel;
		this.detailButton = detailButton;
		this.clickListener = clickListener;
	}

	private HorizontalLayout buildContentArea(Component... components) {
		HorizontalLayout layout = new HorizontalLayout();
		layout.addComponents(components);
		// layout.setMargin(true);
		layout.setWidth("100%");
		layout.setSpacing(true);
		return layout;
	}

	private VerticalLayout buildControlArea(String icon) {
		VerticalLayout layout = new VerticalLayout();
		panelIcon = new Image(null, new ThemeResource(
				icon == null ? "img/doc_64.png" : icon));
		detailButton = new Button("OPEN", clickListener != null ? clickListener
				: new ClickListener() {

					private static final long serialVersionUID = -7407867369607764456L;

					@Override
					public void buttonClick(ClickEvent event) {
						// TODO Auto-generated method stub
						Notification.show("TEST");
					}

				});

		layout.addComponent(panelIcon);
		layout.setComponentAlignment(panelIcon, Alignment.MIDDLE_CENTER);

		layout.setWidth("150px");
		layout.setHeight("170px");

		layout.addComponent(detailButton);
		layout.setComponentAlignment(detailButton, Alignment.BOTTOM_CENTER);

		return layout;
	}

	private VerticalLayout buildDescriptionArea(String value) {
		VerticalLayout layout = new VerticalLayout();

		Label label = new Label(value == null ? "" : value);
		layout.addComponent(label);

		return layout;
	}

	private VerticalLayout buildHeaderArea(String caption) {
		VerticalLayout layout = new VerticalLayout();
		headerLabel = new Label(caption == null ? "" : caption);
		headerLabel.setSizeUndefined();
		headerLabel.addStyleName("h4");
		layout.addComponent(headerLabel);
		layout.setComponentAlignment(headerLabel, Alignment.TOP_LEFT);
		return layout;
	}

	public TTDashPanel init(String... strings) {
		headerArea = buildHeaderArea(strings.length >= 3 ? strings[0] : null);
		controlArea = buildControlArea(strings.length >= 3 ? strings[1] : null);
		descriptionArea = buildDescriptionArea(strings.length >= 3 ? strings[2]
				: null);
		contentArea = buildContentArea(controlArea, descriptionArea);
		footerArea = new VerticalLayout();

		addComponent(headerArea);
		addComponent(new Label("<hr>", ContentMode.HTML));
		addComponent(contentArea);
		addComponent(footerArea);
		setMargin(true);
		footerArea.setHeight("10px");

		// contentArea.setMargin(true);
		// contentArea.setWidth("100%");
		// contentArea.setSpacing(true);

		return this;

	}

}
