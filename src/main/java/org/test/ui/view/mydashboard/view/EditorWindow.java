package org.test.ui.view.mydashboard.view;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.test.ui.view.mydashboard.util.FieldUtil;

import com.vaadin.addon.jpacontainer.fieldfactory.FieldFactory;
import com.vaadin.data.Item;
import com.vaadin.data.validator.BeanValidator;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("deprecation")
public class EditorWindow extends Window implements Button.ClickListener,
		FormFieldFactory {

	public static class EditorSavedEvent extends Component.Event {

		private static final long serialVersionUID = 5835166001144910654L;

		private Item savedItem;

		public EditorSavedEvent(Component source, Item savedItem) {
			super(source);
			this.savedItem = savedItem;
		}

		public Item getSavedItem() {
			return savedItem;
		}
	}

	public interface EditorSavedListener extends Serializable {
		public void editorSaved(EditorSavedEvent event);
	}

	private static final long serialVersionUID = -6108946008084451317L;
	private Button cancelButton;
	private Class<?> clazz;
	private Form editorForm;

	private Item item;

	private Button saveButton;

	public EditorWindow(Class<?> clazz, Item item) {
		super();
		this.clazz = clazz;
		this.item = item;
	}

	public void addListener(EditorSavedListener listener) {
		try {
			Method method = EditorSavedListener.class.getDeclaredMethod(
					"editorSaved", new Class[] { EditorSavedEvent.class });
			addListener(EditorSavedEvent.class, listener, method);
		} catch (final java.lang.NoSuchMethodException e) {
			// This should never happen
			throw new java.lang.RuntimeException(
					"Internal error, editor saved method not found");
		}
	}

	@Override
	public void buttonClick(ClickEvent event) {
		// TODO Auto-generated method stub
		if (event.getButton() == saveButton) {
			editorForm.commit();
			fireEvent(new EditorSavedEvent(this, item));
		} else if (event.getButton() == cancelButton) {
			editorForm.discard();
		}

		close();
	}

	@Override
	public Field<?> createField(Item item, Object propertyId,
			Component uiContext) {
		Field<?> field = null;

		field = (Field<?>) FieldUtil.getField(item, propertyId, uiContext,
				clazz);
		/*
		 * Object result = FieldUtil.getField(propertyId.toString());
		 * 
		 * if (result instanceof Boolean) { field =
		 * DefaultFieldFactory.get().createField(item, propertyId, uiContext); }
		 */
		if (field instanceof TextField) {
			((TextField) field).setNullRepresentation("");
		}

		field.addValidator(new BeanValidator(clazz, propertyId.toString()));

		return field;
	}

	public EditorWindow init() {
		editorForm = new Form();
		editorForm.setFormFieldFactory(this);
		editorForm.setBuffered(true);
		editorForm.setImmediate(true);

		java.lang.reflect.Field[] fields = clazz.getDeclaredFields();
		List<String> list = new LinkedList<String>();

		for (java.lang.reflect.Field field : fields) {
			if (!"serialVersionUID".equals(field.getName())) {
				list.add(field.getName());
			}
		}

		Collections.sort(list);
		FieldFactory factory = new FieldFactory();
//		editorForm.setItemDataSource(item, list);
		editorForm.setItemDataSource(item);

		if (fields.length > 12) {
			setSizeFull();
		}

		saveButton = new Button("Save", this);
		saveButton.addStyleName("default");
		cancelButton = new Button("Cancel", this);

		HorizontalLayout horizontalLayout = new HorizontalLayout();
		horizontalLayout.setWidth("100.0%");
		HorizontalLayout horizontalLayout2 = new HorizontalLayout();
		horizontalLayout2.addComponent(saveButton);
		horizontalLayout2.addComponent(cancelButton);
		horizontalLayout.addComponent(horizontalLayout2);
		horizontalLayout.setComponentAlignment(horizontalLayout2,
				Alignment.MIDDLE_RIGHT);

		editorForm.getFooter().addComponent(horizontalLayout);
		editorForm.getFooter().setWidth("100%");

		VerticalLayout layout = new VerticalLayout();
		layout.setMargin(new MarginInfo(false, true, true, true));
		layout.addComponent(editorForm);

		setContent(layout);
		center();
		setCloseShortcut(KeyCode.ESCAPE, null);

		setResizable(false);
		setModal(true);
		setClosable(true);

		setCaption(clazz.getSimpleName() + this.getClass().getSimpleName());

		return this;
	}

	public void removeListener(EditorSavedListener listener) {
		removeListener(EditorSavedEvent.class, listener);
	}

}
