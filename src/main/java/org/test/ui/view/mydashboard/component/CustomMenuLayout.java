package org.test.ui.view.mydashboard.component;

import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Button.ClickEvent;

public class CustomMenuLayout extends CssLayout implements
		DefaultVooveeComponent<CustomMenuLayout> {

	private static final long serialVersionUID = -1867540268759122125L;
	private Navigator navigator;

	public CustomMenuLayout() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomMenuLayout(Component... children) {
		super(children);
		// TODO Auto-generated constructor stub
	}

	public CustomMenuLayout(Navigator navigator) {
		super();
		this.navigator = navigator;
	}

	public CustomMenuLayout init() {
		addStyleName("menu");

		MenuItem menuItem = new MenuItem(null, "Deal", "deal");
		menuItem.addClickListener(new ClickListener() {
			
			private static final long serialVersionUID = -2619902184459337015L;

			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (!navigator.getState().equals("deal")) {
					navigator.navigateTo("deal");
				}
			}
		});
				
		addComponent(menuItem.init());

//		menuItem = null;
//		navigator = null;

		return this;
	}

}
