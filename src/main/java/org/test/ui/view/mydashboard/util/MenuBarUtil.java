package org.test.ui.view.mydashboard.util;

import java.util.List;

import org.open.tourism.util.ContentType;
import org.open.tourism.util.Domain;
import org.test.ui.view.mydashboard.component.layout.TTDashPanel;
import org.test.ui.view.mydashboard.view.TabView;

import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.VerticalLayout;

public class MenuBarUtil {
	public static GridLayout gridLayout;
	/*
	 * private static String regex; private static String contentType; private
	 * static TabSheet tabSheet; private static MenuBar menuBar;
	 * 
	 * private Integer limit; private Integer partitionSize;
	 * 
	 * public MenuBarUtil(String regex, String contentType, TabSheet tabSheet,
	 * MenuBar menuBar) { super(); MenuBarUtil.regex = regex;
	 * MenuBarUtil.contentType = contentType; MenuBarUtil.tabSheet = tabSheet;
	 * MenuBarUtil.menuBar = menuBar; }
	 */
	public static Integer size = 0;

	private static void createMenu(String label, Component parent, Domain code,
			String contentType, TabSheet tabSheet) {
		if (parent instanceof MenuBar) {
			((MenuBar) parent).addItem(label, new Command() {

				private static final long serialVersionUID = 8134122910257649359L;

				@Override
				public void menuSelected(MenuItem selectedItem) {
					// TODO Auto-generated method stub
					Tab tab = tabSheet.addTab(
							(Component) ContentType.getByCode(code,
									contentType.toUpperCase()), label);
					tab.setClosable(true);

					if (TabView.class.equals(tab.getComponent().getClass())) {
						((TabView<?>) tab.getComponent()).init();
					}

					tabSheet.setSelectedTab(tab);
				}
			});
		}
		/*
		 * if (parent instanceof MenuBar) { menuItem = ((MenuBar)
		 * parent).addItem(label, new Command() {
		 * 
		 * private static final long serialVersionUID = 8134122910257649359L;
		 * 
		 * @Override public void menuSelected(MenuItem selectedItem) { // TODO
		 * Auto-generated method stub Tab tab = tabSheet.addTab( (Component)
		 * ContentType.getByCode(code, contentType.toUpperCase()), label);
		 * tab.setClosable(true);
		 * 
		 * if (TabView.class.equals(tab.getComponent().getClass())) {
		 * ((TabView<?>) tab.getComponent()).init(); }
		 * 
		 * tabSheet.setSelectedTab(tab); } });
		 * 
		 * } else if (parent instanceof MenuItem) { menuItem = ((MenuItem)
		 * parent).addItem(label, new Command() {
		 * 
		 * private static final long serialVersionUID = 1204760889437823024L;
		 * 
		 * @Override public void menuSelected(MenuItem selectedItem) { // TODO
		 * Auto-generated method stub Tab tab = tabSheet.addTab( (Component)
		 * ContentType.getByCode(code, contentType.toUpperCase()), label);
		 * tab.setClosable(true); tabSheet.setSelectedTab(tab);
		 * 
		 * } }); }
		 */
		// menuItem.setStyleName("icon-cog");

	}

	public static void init(String regex, String contentType,
			TabSheet tabSheet, MenuBar menuBar) {
		List<Domain> domains = Domain.has(regex);

		/*
		 * if (domains.size() > limit) { List<List<Domain>> partitions = new
		 * LinkedList<List<Domain>>(); for (int i = 0; i < domains.size(); i +=
		 * partitionSize) { partitions.add(domains.subList(i, i +
		 * Math.min(partitionSize, domains.size() - i))); } }
		 */

		size = domains.size();
		Integer cells = (int) Math.sqrt(size);
		VerticalLayout root = new VerticalLayout();
		gridLayout = new GridLayout(cells, cells);

		MenuItem menuItem = menuBar.addItem("", selectedItem -> {
			Tab tab = tabSheet.addTab(gridLayout);
			tab.setCaption("DASH");
			tab.setClosable(true);
			tabSheet.setSelectedTab(tab);
		});

		menuItem.setStyleName("icon-home");

		domains.stream()
				.forEach(
						(it) -> {
							createMenu(it.getClazz().getSimpleName(), menuBar,
									it, contentType, tabSheet);

							TTDashPanel dashPanel = new TTDashPanel(
									it.getClazz().getSimpleName(),
									null,
									"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,",
									event -> {
										// TODO Auto-generated method stub
										Tab tab = tabSheet.addTab(
												(Component) ContentType
														.getByCode(
																it,
																contentType
																		.toUpperCase()),
												it.getClazz().getSimpleName());
										tab.setClosable(true);

										if (TabView.class.equals(tab
												.getComponent().getClass())) {
											((TabView<?>) tab.getComponent())
													.init();
										}

										tabSheet.setSelectedTab(tab);
									});
							gridLayout.addComponent(dashPanel);
						});

		gridLayout.setSpacing(true);
		gridLayout.setSizeUndefined();
		gridLayout.setWidth("100%");
		root.setSizeFull();
		root.addComponent(gridLayout);

		Tab tab = tabSheet.addTab(gridLayout);
		tab.setCaption("DASH");
		tab.setClosable(true);
		tabSheet.setSelectedTab(tab);
	}

}
