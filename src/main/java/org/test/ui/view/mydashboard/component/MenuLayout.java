package org.test.ui.view.mydashboard.component;

import java.util.Iterator;

import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.navigator.Navigator;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;

public class MenuLayout extends CssLayout {

	private static final long serialVersionUID = 4404328839573114886L;

	private JPAContainer<?> container;
	private Navigator navigator;

	public MenuLayout() {
		super();

	}

	public MenuLayout(JPAContainer<?> container) {
		super();
		this.container = container;
	}

	public MenuLayout(JPAContainer<?> container, Navigator navigator) {
		super();
		this.container = container;
		this.navigator = navigator;
	}

	public JPAContainer<?> getContainer() {
		return container;
	}

	public MenuLayout init() {
		removeAllComponents();
		removeStyleName("menu");

		addStyleName("menu");

		if (container != null) {
			container
					.getItemIds()
					.stream()
					.forEach(
							(it) -> {
								MenuItem menuItem = new MenuItem();
								processBuildMenuItem(it, menuItem);

								if (navigator != null) {
									menuItem = processBuildMenuItemClick(menuItem);

									if (navigator.getState().equals(
											menuItem.getLink())
											&& menuItem.getSelected() == false) {
										menuItem.setSelected(true);
									}
								}

								menuItem.init();
								addComponent(menuItem);

							});
		}

		return this;
	}

	private void processBuildMenuItem(Object it, MenuItem menuItem) {
		EntityItem<?> entityItem = container.getItem(it);

		if (entityItem.getItemProperty("label").getValue() != null) {
			menuItem.setLabel(entityItem.getItemProperty("label").getValue()
					.toString());
		} else {
			menuItem.setLabel(entityItem.getItemProperty("name").getValue()
					.toString());
		}

		menuItem.setLink(entityItem.getItemProperty("link").getValue()
				.toString());

		if (entityItem.getItemProperty("css_class").getValue() != null) {
			menuItem.addStyleName(entityItem.getItemProperty("css_class")
					.getValue().toString());
		}
		if (entityItem.getItemProperty("image").getValue() != null) {
			menuItem.setImage(entityItem.getItemProperty("image").getValue()
					.toString());
		}
		if (entityItem.getItemProperty("badge").getValue() != null) {
			menuItem.setBadge(entityItem.getItemProperty("badge").getValue()
					.toString());
		}

	}

	private MenuItem processBuildMenuItemClick(MenuItem menuItem) {
		menuItem.addClickListener((e) -> {

			if (!navigator.getState().equals(menuItem.getLink())) {
				navigator.navigateTo(menuItem.getLink());

				Iterator<Component> iterator = iterator();
				while (iterator.hasNext()) {
					Component component = iterator.next();
					component.removeStyleName("selected");
				}

				menuItem.removeBadge();
				menuItem.setSelected(true);
			}

		});

		return menuItem;
	}

	public void setContainer(JPAContainer<?> container) {
		this.container = container;
	}

}
