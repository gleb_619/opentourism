package org.test.ui.view.mydashboard.component;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.NativeButton;

public class MenuItem extends NativeButton {

	private static final long serialVersionUID = -5754587339497350336L;

	private String badge;
	private String image;
	private String label;
	private String link;
	private Boolean selected = false;

	public MenuItem() {
		super();

	}

	public MenuItem(String label) {
		super();
		this.label = label;
	}

	public MenuItem(String badge, String label) {
		super();
		this.badge = badge;
		this.label = label;
	}

	public MenuItem(String badge, String label, String link) {
		super();
		this.badge = badge;
		this.label = label;
		this.link = link;
	}

	public MenuItem(String label, String badge, String image, Boolean selected) {
		super();
		this.label = label;
		this.badge = badge;
		this.image = image;
		this.selected = selected;
	}

	public String getBadge() {
		return badge;
	}

	public String getImage() {
		return image;
	}

	public String getLabel() {
		return label;
	}

	public String getLink() {
		return link;
	}

	public Boolean getSelected() {
		return selected;
	}

	public MenuItem init() {
		if (image != null) {
			setIcon(new ThemeResource(image));
		}

		if (selected) {
			addStyleName("selected");
		}

		setErrorHandler((e) -> {
			System.out.println(this.getClass().getName() + " " + e.getThrowable().getLocalizedMessage());
		});
		
		setHeader();

		return this;
	}

	public void removeBadge() {
		this.badge = null;
		setHeader();
	}

	public void setBadge(String badge) {
		this.badge = badge;
	}

	public void setHeader() {
		if (badge != null) {
			setHtmlContentAllowed(true);
			setCaption(String.format("%s <span class=\"badge\">%s</span>",
					label, badge));
		} else {
			setCaption(label);
		}
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setSelected(Boolean selected) {
		this.selected = selected;
		if (selected) {
			addStyleName("selected");
		}
	}

}
