package org.test.launcher;

import org.open.tourism.service.MainService;
import org.voovee.crm.domain.entity.Deal;
import org.voovee.crm.domain.entity.Tag;

public class TestC {

	public static void main(String[] args) {
		MainService<Deal> mainService = new MainService<>(Deal.class);
		mainService.findAll();

		Deal deal = new Deal("TestDeal3");
		deal.getTags().add(new Tag("testTag2"));
		deal.getTags().add(new Tag("testTags2"));

		deal.getTags().forEach(System.out::println);

		mainService.persist(deal);
		System.out.println("---------" + deal.getId() + "--------------");
		deal = mainService.findByExample(deal);

		if (deal != null) {
			deal.getTags().forEach(System.out::println);
		}
	}

}
