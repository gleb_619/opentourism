package org.voovee.crm.domain.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.voovee.crm.domain.sentity.DefaultEntityCRM;

@Entity
public class Account extends DefaultEntityCRM {

	private static final long serialVersionUID = -4748952893856904474L;

	private Integer id;

	private Dict accountCategory;

	private Dict accountType;
	private Set<Address> addresses = new LinkedHashSet<Address>();
	private Set<BillingInformation> billingInformations = new LinkedHashSet<BillingInformation>();
	private String code;

	private Set<Communication> communications = new LinkedHashSet<Communication>();
	private Set<Contact> contacts = new LinkedHashSet<Contact>();
	private Contact defaultContact;
	private Dict generalCategoryType;

	private Dict generalEmployeeNumber;
	private Dict generalIndastry;
	private Dict generalOwnership;

	private Set<Structure> structure = new LinkedHashSet<Structure>();

	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Account(Integer id, Dict accountCategory, Dict accountType,
			Set<Address> addresses,
			Set<BillingInformation> billingInformations, String code,
			Set<Communication> communications, Set<Contact> contacts,
			Contact defaultContact, Dict generalCategoryType,
			Dict generalEmployeeNumber, Dict generalIndastry,
			Dict generalOwnership, Set<Structure> structure) {
		super();
		this.id = id;
		this.accountCategory = accountCategory;
		this.accountType = accountType;
		this.addresses = addresses;
		this.billingInformations = billingInformations;
		this.code = code;
		this.communications = communications;
		this.contacts = contacts;
		this.defaultContact = defaultContact;
		this.generalCategoryType = generalCategoryType;
		this.generalEmployeeNumber = generalEmployeeNumber;
		this.generalIndastry = generalIndastry;
		this.generalOwnership = generalOwnership;
		this.structure = structure;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getAccountCategory() {
		return accountCategory;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getAccountType() {
		return accountType;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<Address> getAddresses() {
		return addresses;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<BillingInformation> getBillingInformations() {
		return billingInformations;
	}

	@Column(length = 512)
	public String getCode() {
		return code;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<Communication> getCommunications() {
		return communications;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
	public Set<Contact> getContacts() {
		return contacts;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Contact getDefaultContact() {
		return defaultContact;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<Structure> getStructure() {
		return structure;
	}

	public void setAccountCategory(Dict accountCategory) {
		this.accountCategory = accountCategory;
	}

	public void setAccountType(Dict accountType) {
		this.accountType = accountType;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	public void setBillingInformations(
			Set<BillingInformation> billingInformations) {
		this.billingInformations = billingInformations;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCommunications(Set<Communication> communications) {
		this.communications = communications;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public void setDefaultContact(Contact defaultContact) {
		this.defaultContact = defaultContact;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setStructure(Set<Structure> structure) {
		this.structure = structure;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getGeneralCategoryType() {
		return generalCategoryType;
	}

	public void setGeneralCategoryType(Dict generalCategoryType) {
		this.generalCategoryType = generalCategoryType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getGeneralEmployeeNumber() {
		return generalEmployeeNumber;
	}

	public void setGeneralEmployeeNumber(Dict generalEmployeeNumber) {
		this.generalEmployeeNumber = generalEmployeeNumber;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getGeneralIndastry() {
		return generalIndastry;
	}

	public void setGeneralIndastry(Dict generalIndastry) {
		this.generalIndastry = generalIndastry;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getGeneralOwnership() {
		return generalOwnership;
	}

	public void setGeneralOwnership(Dict generalOwnership) {
		this.generalOwnership = generalOwnership;
	}

}
