package org.voovee.crm.domain.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Payment implements Serializable {

	private static final long serialVersionUID = 5948214589498239259L;

	private Integer id;

	private Date plannedOn;
	private BigDecimal plannedSum;

	private Date paymentOn;
	private BigDecimal paymentSum;

	private Dict condition;
	private Dict paymentPeriod;
	private Dict type;
	private BigDecimal paymentAmount;
	private BigDecimal paymentAmountBase;
	private BigDecimal paymentTax;
	private Double paymentTaxPersentage;
	private BigDecimal paymentDiscount;
	private Double paymentDiscountPersentage;
	private BigDecimal paymentCommission;
	private Double paymentCommissionPersentage;
	private BigDecimal margin;
	private Double marginPersentage;

	public Payment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Payment(Integer id, Date plannedOn, BigDecimal plannedSum,
			Date paymentOn, BigDecimal paymentSum, Dict condition,
			Dict paymentPeriod, Dict type, BigDecimal paymentAmount,
			BigDecimal paymentAmountBase, BigDecimal paymentTax,
			Double paymentTaxPersentage, BigDecimal paymentDiscount,
			Double paymentDiscountPersentage, BigDecimal paymentCommission,
			Double paymentCommissionPersentage, BigDecimal margin,
			Double marginPersentage) {
		super();
		this.id = id;
		this.plannedOn = plannedOn;
		this.plannedSum = plannedSum;
		this.paymentOn = paymentOn;
		this.paymentSum = paymentSum;
		this.condition = condition;
		this.paymentPeriod = paymentPeriod;
		this.type = type;
		this.paymentAmount = paymentAmount;
		this.paymentAmountBase = paymentAmountBase;
		this.paymentTax = paymentTax;
		this.paymentTaxPersentage = paymentTaxPersentage;
		this.paymentDiscount = paymentDiscount;
		this.paymentDiscountPersentage = paymentDiscountPersentage;
		this.paymentCommission = paymentCommission;
		this.paymentCommissionPersentage = paymentCommissionPersentage;
		this.margin = margin;
		this.marginPersentage = marginPersentage;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Temporal(TemporalType.DATE)
	public Date getPlannedOn() {
		return plannedOn;
	}

	public void setPlannedOn(Date plannedOn) {
		this.plannedOn = plannedOn;
	}

	public BigDecimal getPlannedSum() {
		return plannedSum;
	}

	public void setPlannedSum(BigDecimal plannedSum) {
		this.plannedSum = plannedSum;
	}

	@Temporal(TemporalType.DATE)
	public Date getPaymentOn() {
		return paymentOn;
	}

	public void setPaymentOn(Date paymentOn) {
		this.paymentOn = paymentOn;
	}

	public BigDecimal getPaymentSum() {
		return paymentSum;
	}

	public void setPaymentSum(BigDecimal paymentSum) {
		this.paymentSum = paymentSum;
	}

	public Dict getCondition() {
		return condition;
	}

	public void setCondition(Dict condition) {
		this.condition = condition;
	}

	public Dict getPaymentPeriod() {
		return paymentPeriod;
	}

	public void setPaymentPeriod(Dict paymentPeriod) {
		this.paymentPeriod = paymentPeriod;
	}

	public Dict getType() {
		return type;
	}

	public void setType(Dict type) {
		this.type = type;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public BigDecimal getPaymentAmountBase() {
		return paymentAmountBase;
	}

	public void setPaymentAmountBase(BigDecimal paymentAmountBase) {
		this.paymentAmountBase = paymentAmountBase;
	}

	public BigDecimal getPaymentTax() {
		return paymentTax;
	}

	public void setPaymentTax(BigDecimal paymentTax) {
		this.paymentTax = paymentTax;
	}

	public Double getPaymentTaxPersentage() {
		return paymentTaxPersentage;
	}

	public void setPaymentTaxPersentage(Double paymentTaxPersentage) {
		this.paymentTaxPersentage = paymentTaxPersentage;
	}

	public BigDecimal getPaymentDiscount() {
		return paymentDiscount;
	}

	public void setPaymentDiscount(BigDecimal paymentDiscount) {
		this.paymentDiscount = paymentDiscount;
	}

	public Double getPaymentDiscountPersentage() {
		return paymentDiscountPersentage;
	}

	public void setPaymentDiscountPersentage(Double paymentDiscountPersentage) {
		this.paymentDiscountPersentage = paymentDiscountPersentage;
	}

	public BigDecimal getPaymentCommission() {
		return paymentCommission;
	}

	public void setPaymentCommission(BigDecimal paymentCommission) {
		this.paymentCommission = paymentCommission;
	}

	public Double getPaymentCommissionPersentage() {
		return paymentCommissionPersentage;
	}

	public void setPaymentCommissionPersentage(
			Double paymentCommissionPersentage) {
		this.paymentCommissionPersentage = paymentCommissionPersentage;
	}

	public BigDecimal getMargin() {
		return margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public Double getMarginPersentage() {
		return marginPersentage;
	}

	public void setMarginPersentage(Double marginPersentage) {
		this.marginPersentage = marginPersentage;
	}

}
