package org.voovee.crm.domain.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.voovee.crm.domain.sentity.DefaultEntityAssign;

@Entity
public class Bill extends DefaultEntityAssign {

	private static final long serialVersionUID = 2966243600232488762L;

	private Integer id;

	private Dict condition;
	private Dict conditionType;

	private Deal deal;
	private Document document;

	private Date dPaymentOn;
	private Date dSignOn;
	private Boolean paid;

	private Dict paymentPeriod;
	private BigDecimal saleAmount;
	private BigDecimal saleAmountBase;

	private BigDecimal saleCommission;
	private Double saleCommissionPersentage;
	private BigDecimal saleDiscount;
	private Double saleDiscountPersentage;
	private BigDecimal saleTax;
	private Double saleTaxPersentage;

	private Account supplier;

	public Bill() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Bill(Integer id, Dict condition, Dict conditionType, Deal deal,
			Document document, Date dPaymentOn, Date dSignOn, Boolean paid,
			Dict paymentPeriod, BigDecimal saleAmount,
			BigDecimal saleAmountBase, BigDecimal saleCommission,
			Double saleCommissionPersentage, BigDecimal saleDiscount,
			Double saleDiscountPersentage, BigDecimal saleTax,
			Double saleTaxPersentage, Account supplier) {
		super();
		this.id = id;
		this.condition = condition;
		this.conditionType = conditionType;
		this.deal = deal;
		this.document = document;
		this.dPaymentOn = dPaymentOn;
		this.dSignOn = dSignOn;
		this.paid = paid;
		this.paymentPeriod = paymentPeriod;
		this.saleAmount = saleAmount;
		this.saleAmountBase = saleAmountBase;
		this.saleCommission = saleCommission;
		this.saleCommissionPersentage = saleCommissionPersentage;
		this.saleDiscount = saleDiscount;
		this.saleDiscountPersentage = saleDiscountPersentage;
		this.saleTax = saleTax;
		this.saleTaxPersentage = saleTaxPersentage;
		this.supplier = supplier;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getCondition() {
		return condition;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getConditionType() {
		return conditionType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Deal getDeal() {
		return deal;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Document getDocument() {
		return document;
	}

	@Temporal(TemporalType.DATE)
	public Date getdPaymentOn() {
		return dPaymentOn;
	}

	@Temporal(TemporalType.DATE)
	public Date getdSignOn() {
		return dSignOn;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public Boolean getPaid() {
		return paid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getPaymentPeriod() {
		return paymentPeriod;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Account getSupplier() {
		return supplier;
	}

	public void setCondition(Dict condition) {
		this.condition = condition;
	}

	public void setConditionType(Dict conditionType) {
		this.conditionType = conditionType;
	}

	public void setDeal(Deal deal) {
		this.deal = deal;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public void setdPaymentOn(Date dPaymentOn) {
		this.dPaymentOn = dPaymentOn;
	}

	public void setdSignOn(Date dSignOn) {
		this.dSignOn = dSignOn;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	public void setPaymentPeriod(Dict paymentPeriod) {
		this.paymentPeriod = paymentPeriod;
	}

	public void setSupplier(Account supplier) {
		this.supplier = supplier;
	}

	public BigDecimal getSaleAmount() {
		return saleAmount;
	}

	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}

	public BigDecimal getSaleAmountBase() {
		return saleAmountBase;
	}

	public void setSaleAmountBase(BigDecimal saleAmountBase) {
		this.saleAmountBase = saleAmountBase;
	}

	public BigDecimal getSaleCommission() {
		return saleCommission;
	}

	public void setSaleCommission(BigDecimal saleCommission) {
		this.saleCommission = saleCommission;
	}

	public Double getSaleCommissionPersentage() {
		return saleCommissionPersentage;
	}

	public void setSaleCommissionPersentage(Double saleCommissionPersentage) {
		this.saleCommissionPersentage = saleCommissionPersentage;
	}

	public BigDecimal getSaleDiscount() {
		return saleDiscount;
	}

	public void setSaleDiscount(BigDecimal saleDiscount) {
		this.saleDiscount = saleDiscount;
	}

	public Double getSaleDiscountPersentage() {
		return saleDiscountPersentage;
	}

	public void setSaleDiscountPersentage(Double saleDiscountPersentage) {
		this.saleDiscountPersentage = saleDiscountPersentage;
	}

	public BigDecimal getSaleTax() {
		return saleTax;
	}

	public void setSaleTax(BigDecimal saleTax) {
		this.saleTax = saleTax;
	}

	public Double getSaleTaxPersentage() {
		return saleTaxPersentage;
	}

	public void setSaleTaxPersentage(Double saleTaxPersentage) {
		this.saleTaxPersentage = saleTaxPersentage;
	}

}
