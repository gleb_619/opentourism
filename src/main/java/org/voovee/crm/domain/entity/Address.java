package org.voovee.crm.domain.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

@Entity
public class Address extends DefaultEntityObject {

	private static final long serialVersionUID = -2526480972507380503L;

	private Integer id;

	private Integer index;
	private Dict type;

	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Address(Integer id, Dict type, Integer index) {
		super();
		this.id = id;
		this.type = type;
		this.index = index;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public Integer getIndex() {
		return index;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getType() {
		return type;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public void setType(Dict type) {
		this.type = type;
	}

}
