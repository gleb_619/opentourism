package org.voovee.crm.domain.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

@Entity
public class TeamMember extends DefaultEntityObject {

	private static final long serialVersionUID = 4309725761279481015L;

	private Contact contact;

	private Integer id;
	private Dict role;

	public TeamMember() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TeamMember(Contact contact, Dict role) {
		super();
		this.contact = contact;
		this.role = role;
	}

	public TeamMember(Integer id, Contact contact, Dict role) {
		super();
		this.id = id;
		this.contact = contact;
		this.role = role;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Contact getContact() {
		return contact;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getRole() {
		return role;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setRole(Dict role) {
		this.role = role;
	}

}
