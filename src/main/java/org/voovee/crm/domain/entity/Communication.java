package org.voovee.crm.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

@Entity
public class Communication extends DefaultEntityObject {

	private static final long serialVersionUID = -4597453016542152561L;

	private Integer id;

	private Dict type;
	private String value;

	public Communication() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Communication(Integer id, Dict type, String value) {
		super();
		this.id = id;
		this.type = type;
		this.value = value;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getType() {
		return type;
	}

	@Column(length = 512)
	public String getValue() {
		return value;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setType(Dict type) {
		this.type = type;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
