package org.voovee.crm.domain.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

@Entity
public class Company extends DefaultEntityObject {

	private static final long serialVersionUID = 6750726445106268073L;

	private Integer id;
	private String name;
	private Set<VUser> users = new LinkedHashSet<VUser>();

	public Company() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Company(Integer id, String name, Set<VUser> users) {
		super();
		this.id = id;
		this.name = name;
		this.users = users;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@Column(length = 512)
	public String getName() {
		return name;
	}

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	public Set<VUser> getUsers() {
		return users;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setUsers(Set<VUser> users) {
		this.users = users;
	}

}
