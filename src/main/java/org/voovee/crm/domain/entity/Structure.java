package org.voovee.crm.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

@Entity
public class Structure extends DefaultEntityObject {

	private static final long serialVersionUID = 5933397085269320439L;

	private Dict departament;

	private String division;
	private Contact head;
	private Integer id;
	private Structure structure;

	public Structure() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Structure(Integer id, String division, Dict departament,
			Contact head, Structure structure) {
		super();
		this.id = id;
		this.division = division;
		this.departament = departament;
		this.head = head;
		this.structure = structure;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getDepartament() {
		return departament;
	}

	@Column(length = 512)
	public String getDivision() {
		return division;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Contact getHead() {
		return head;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@OneToOne(fetch = FetchType.LAZY)
	public Structure getStructure() {
		return structure;
	}

	public void setDepartament(Dict departament) {
		this.departament = departament;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setHead(Contact head) {
		this.head = head;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setStructure(Structure structure) {
		this.structure = structure;
	}

}
