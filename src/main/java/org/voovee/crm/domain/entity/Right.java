package org.voovee.crm.domain.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Right implements Serializable {

	private static final long serialVersionUID = -4570898783915028155L;

	private Integer id;

	private Boolean read;
	private Boolean edit;
	private Boolean delete;
	private Boolean visible;

	public Right() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Right(Integer id, Boolean read, Boolean edit, Boolean delete,
			Boolean visible) {
		super();
		this.id = id;
		this.read = read;
		this.edit = edit;
		this.delete = delete;
		this.visible = visible;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Boolean getRead() {
		return read;
	}

	public void setRead(Boolean read) {
		this.read = read;
	}

	public Boolean getEdit() {
		return edit;
	}

	public void setEdit(Boolean edit) {
		this.edit = edit;
	}

	public Boolean getDelete() {
		return delete;
	}

	public void setDelete(Boolean delete) {
		this.delete = delete;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

}
