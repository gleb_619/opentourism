package org.voovee.crm.domain.entity.listner;

import java.util.UUID;

import javax.persistence.PrePersist;

import org.voovee.crm.domain.sentity.DefaultEntity;

public class DefaultEntityListner {

	@PrePersist
	public void prePersist(Object object) {
		if (object instanceof DefaultEntity) {
			((DefaultEntity) object).setKey(UUID.randomUUID().toString());
		}
	}
	/*
	 * @PostPersist public void postPersist(Object object) {
	 * System.out.println(object); System.out.println(object.toString()); }
	 * 
	 * @PreDestroy public void preDestroy() { System.out.println("Destroy"); }
	 * 
	 * @PostConstruct public void postConstruct() {
	 * System.out.println("Construct"); }
	 */

}
