package org.voovee.crm.domain.entity;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

@Entity
public class VUser extends DefaultEntityObject {

	private static final long serialVersionUID = -1184578463635803745L;

	private Company company;
	private Byte[] customSalt;
	private Byte[] customSaltAlternative;
	private Integer id;
	private String name;
	private Byte[] pass;
	private Set<Role> role = new LinkedHashSet<Role>();

	public VUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public VUser(Company company, Byte[] customSalt,
			Byte[] customSaltAlternative, Integer id, String name, Byte[] pass,
			Set<Role> role) {
		super();
		this.company = company;
		this.customSalt = customSalt;
		this.customSaltAlternative = customSaltAlternative;
		this.id = id;
		this.name = name;
		this.pass = pass;
		this.role = role;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Company getCompany() {
		return company;
	}

	@Lob
	public Byte[] getCustomSalt() {
		return customSalt;
	}

	@Lob
	public Byte[] getCustomSaltAlternative() {
		return customSaltAlternative;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@Column(length = 512)
	public String getName() {
		return name;
	}

	@Lob
	public Byte[] getPass() {
		return pass;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCustomSalt(Byte[] customSalt) {
		this.customSalt = customSalt;
	}

	public void setCustomSaltAlternative(Byte[] customSaltAlternative) {
		this.customSaltAlternative = customSaltAlternative;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPass(Byte[] pass) {
		this.pass = pass;
	}

	public Set<Role> getRole() {
		return role;
	}

	public void setRole(Set<Role> role) {
		this.role = role;
	}

}
