package org.voovee.crm.domain.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

@Entity
public class DealMember extends DefaultEntityObject {

	private static final long serialVersionUID = 1803317198969084433L;

	private Contact contact;

	private Integer id;
	private Dict role;
	private Double chance;

	public DealMember() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DealMember(Contact contact, Integer id, Dict role, Double chance) {
		super();
		this.contact = contact;
		this.id = id;
		this.role = role;
		this.chance = chance;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Contact getContact() {
		return contact;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getRole() {
		return role;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setRole(Dict role) {
		this.role = role;
	}

	public Double getChance() {
		return chance;
	}

	public void setChance(Double chance) {
		this.chance = chance;
	}

}
