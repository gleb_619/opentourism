package org.voovee.crm.domain.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.voovee.crm.domain.sentity.DefaultEntityCRM;

@Entity
public class Contact extends DefaultEntityCRM {

	private static final long serialVersionUID = -2632705197126664880L;

	private Integer id;

	private Account account;

	private Date genaralBirthday;
	private String firstName;
	private Boolean genaralGender;

	private String lastName;
	private String jobPositionFull;

	private String secondName;

	public Contact() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Contact(Integer id, Account account, Date genaralBirthday,
			String firstName, Boolean genaralGender, String lastName,
			String jobPositionFull, String secondName) {
		super();
		this.id = id;
		this.account = account;
		this.genaralBirthday = genaralBirthday;
		this.firstName = firstName;
		this.genaralGender = genaralGender;
		this.lastName = lastName;
		this.jobPositionFull = jobPositionFull;
		this.secondName = secondName;
	}

	public Contact(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Account getAccount() {
		return account;
	}

	@Column(length = 512)
	public String getFirstName() {
		return firstName;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@Column(length = 512)
	public String getLastName() {
		return lastName;
	}

	@Column(length = 512)
	public String getSecondName() {
		return secondName;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	@Temporal(TemporalType.DATE)
	public Date getGenaralBirthday() {
		return genaralBirthday;
	}

	public void setGenaralBirthday(Date genaralBirthday) {
		this.genaralBirthday = genaralBirthday;
	}

	public Boolean getGenaralGender() {
		return genaralGender;
	}

	public void setGenaralGender(Boolean genaralGender) {
		this.genaralGender = genaralGender;
	}

	public String getJobPositionFull() {
		return jobPositionFull;
	}

	public void setJobPositionFull(String jobPositionFull) {
		this.jobPositionFull = jobPositionFull;
	}

}
