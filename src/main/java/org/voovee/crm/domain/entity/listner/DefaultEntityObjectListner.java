package org.voovee.crm.domain.entity.listner;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

public class DefaultEntityObjectListner {

	@PrePersist
	public void prePersist(Object object) {
		if (object instanceof DefaultEntityObject) {
			setDate((DefaultEntityObject) object);
		}
	}

	@PreUpdate
	public void PreUpdate(Object object) {
		if (object instanceof DefaultEntityObject) {
			updateDate((DefaultEntityObject) object);
		}
	}

	private void setDate(DefaultEntityObject defaultEntityObject) {
		defaultEntityObject.setdCreateOn(new Date());
		defaultEntityObject.setdModifyOn(new Date());
	}

	private void updateDate(DefaultEntityObject defaultEntityObject) {
		defaultEntityObject.setdModifyOn(new Date());
	}

}
