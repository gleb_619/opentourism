package org.voovee.crm.domain.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

@Entity
public class Product extends DefaultEntityObject {

	private static final long serialVersionUID = -6222826504379000447L;

	private Integer amount;

	private BigDecimal discount;

	private Double discountPersentage;
	private Good good;
	private Integer id;
	private Date openOn;
	private BigDecimal price;
	private Dict result;
	private Boolean service;

	private BigDecimal sum;

	private BigDecimal tax;
	private Double taxPersentage;

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Product(Integer id, Good good, BigDecimal price,
			BigDecimal discount, Double discountPersentage, BigDecimal tax,
			Double taxPersentage, Integer amount, BigDecimal sum,
			Boolean service, Date openOn, Dict result) {
		super();
		this.id = id;
		this.good = good;
		this.price = price;
		this.discount = discount;
		this.discountPersentage = discountPersentage;
		this.tax = tax;
		this.taxPersentage = taxPersentage;
		this.amount = amount;
		this.sum = sum;
		this.service = service;
		this.openOn = openOn;
		this.result = result;
	}

	public Integer getAmount() {
		return amount;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public Double getDiscountPersentage() {
		return discountPersentage;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Good getGood() {
		return good;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@Temporal(TemporalType.DATE)
	public Date getOpenOn() {
		return openOn;
	}

	public BigDecimal getPrice() {
		return price;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getResult() {
		return result;
	}

	public Boolean getService() {
		return service;
	}

	public BigDecimal getSum() {
		return sum;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public Double getTaxPersentage() {
		return taxPersentage;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public void setDiscountPersentage(Double discountPersentage) {
		this.discountPersentage = discountPersentage;
	}

	public void setGood(Good good) {
		this.good = good;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setOpenOn(Date openOn) {
		this.openOn = openOn;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public void setResult(Dict result) {
		this.result = result;
	}

	public void setService(Boolean service) {
		this.service = service;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public void setTaxPersentage(Double taxPersentage) {
		this.taxPersentage = taxPersentage;
	}

}
