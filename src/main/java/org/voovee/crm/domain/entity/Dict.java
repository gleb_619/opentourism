package org.voovee.crm.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

@Entity
public class Dict extends DefaultEntityObject {

	private static final long serialVersionUID = -8153924862428772619L;

	private Integer id;
	private String type;
	private String typeAdditional;
	private String value;
	private String valueAdditional;
	private String valueEn;
	private String valueKz;
	private String valueRu;
	private String valueUa;
	private String valueBe;

	public Dict() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Dict(Integer id, String type, String typeAdditional, String value,
			String valueAdditional, String valueEn, String valueKz,
			String valueRu, String valueUa, String valueBe) {
		super();
		this.id = id;
		this.type = type;
		this.typeAdditional = typeAdditional;
		this.value = value;
		this.valueAdditional = valueAdditional;
		this.valueEn = valueEn;
		this.valueKz = valueKz;
		this.valueRu = valueRu;
		this.valueUa = valueUa;
		this.valueBe = valueBe;
	}

	@Id
	@GeneratedValue
	public Integer getId() {
		return id;
	}

	@Column(length = 512)
	public String getType() {
		return type;
	}

	@Column(length = 512)
	public String getValue() {
		return value;
	}

	public String getValueEn() {
		return valueEn;
	}

	public String getValueKz() {
		return valueKz;
	}

	public String getValueRu() {
		return valueRu;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setValueEn(String valueEn) {
		this.valueEn = valueEn;
	}

	public void setValueKz(String valueKz) {
		this.valueKz = valueKz;
	}

	public void setValueRu(String valueRu) {
		this.valueRu = valueRu;
	}

	public String getValueUa() {
		return valueUa;
	}

	public void setValueUa(String valueUa) {
		this.valueUa = valueUa;
	}

	public String getValueBe() {
		return valueBe;
	}

	public void setValueBe(String valueBe) {
		this.valueBe = valueBe;
	}

	public String getTypeAdditional() {
		return typeAdditional;
	}

	public void setTypeAdditional(String typeAdditional) {
		this.typeAdditional = typeAdditional;
	}

	public String getValueAdditional() {
		return valueAdditional;
	}

	public void setValueAdditional(String valueAdditional) {
		this.valueAdditional = valueAdditional;
	}

}
