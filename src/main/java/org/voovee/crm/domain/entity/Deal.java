package org.voovee.crm.domain.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.voovee.crm.domain.sentity.DefaultEntityAssign;

@Entity
public class Deal extends DefaultEntityAssign {

	private static final long serialVersionUID = 3686407482750368277L;

	private Integer id;

	private Set<Account> contrAccounts = new LinkedHashSet<Account>();
	private Set<Product> contrProducts = new LinkedHashSet<Product>();

	private Set<DealMember> dealMembers = new LinkedHashSet<DealMember>();
	private Dict direction;

	private Set<Bill> bills = new LinkedHashSet<Bill>();
	private Set<Document> documents = new LinkedHashSet<Document>();
	private Set<Payment> payments = new LinkedHashSet<Payment>();

	private Right right;

	private Date dOpen;
	private Date dClose;
	private Date dStart;
	private Date dStop;
	private BigDecimal margin;

	private Double marginPersentage;
	private Dict orderStage;
	private BigDecimal payAdjustment;
	private Double payAjustmentPersentage;

	private Dict paymentType;
	private BigDecimal payDiscount;
	private Double payDiscountPersentage;

	private Set<Product> payProducts = new LinkedHashSet<Product>();
	private BigDecimal paySum;

	private BigDecimal payTax;
	private Double payTaxPersentage;
	private Dict saleCategory;
	private Dict saleType;
	private Double saleChance;
	private BigDecimal saleClientHave;
	private Boolean saleDecline;
	private Boolean saleFrozen;

	private Boolean saleInProcess;
	private Dict source;

	private Boolean saleOver;
	private Boolean saleStart;
	private BigDecimal saleSum;
	private Set<TeamMember> teamMembers = new LinkedHashSet<TeamMember>();
	private Account teamAccount;

	public Deal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Deal(Integer id, Set<Account> contrAccounts,
			Set<Product> contrProducts, Set<DealMember> dealMembers,
			Dict direction, Set<Bill> bills, Set<Document> documents,
			Set<Payment> payments, Right right, Date dOpen, Date dClose,
			Date dStart, Date dStop, BigDecimal margin,
			Double marginPersentage, Dict orderStage, BigDecimal payAdjustment,
			Double payAjustmentPersentage, Dict paymentType,
			BigDecimal payDiscount, Double payDiscountPersentage,
			Set<Product> payProducts, BigDecimal paySum, BigDecimal payTax,
			Double payTaxPersentage, Dict saleCategory, Dict saleType,
			Double saleChance, BigDecimal saleClientHave, Boolean saleDecline,
			Boolean saleFrozen, Boolean saleInProcess, Dict source,
			Boolean saleOver, Boolean saleStart, BigDecimal saleSum,
			Set<TeamMember> teamMembers, Account teamAccount) {
		super();
		this.id = id;
		this.contrAccounts = contrAccounts;
		this.contrProducts = contrProducts;
		this.dealMembers = dealMembers;
		this.direction = direction;
		this.bills = bills;
		this.documents = documents;
		this.payments = payments;
		this.right = right;
		this.dOpen = dOpen;
		this.dClose = dClose;
		this.dStart = dStart;
		this.dStop = dStop;
		this.margin = margin;
		this.marginPersentage = marginPersentage;
		this.orderStage = orderStage;
		this.payAdjustment = payAdjustment;
		this.payAjustmentPersentage = payAjustmentPersentage;
		this.paymentType = paymentType;
		this.payDiscount = payDiscount;
		this.payDiscountPersentage = payDiscountPersentage;
		this.payProducts = payProducts;
		this.paySum = paySum;
		this.payTax = payTax;
		this.payTaxPersentage = payTaxPersentage;
		this.saleCategory = saleCategory;
		this.saleType = saleType;
		this.saleChance = saleChance;
		this.saleClientHave = saleClientHave;
		this.saleDecline = saleDecline;
		this.saleFrozen = saleFrozen;
		this.saleInProcess = saleInProcess;
		this.source = source;
		this.saleOver = saleOver;
		this.saleStart = saleStart;
		this.saleSum = saleSum;
		this.teamMembers = teamMembers;
		this.teamAccount = teamAccount;
	}

	public Deal(String title) {
		super(title);
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<Bill> getBills() {
		return bills;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<Account> getContrAccounts() {
		return contrAccounts;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<Product> getContrProducts() {
		return contrProducts;
	}

	@Temporal(TemporalType.DATE)
	public Date getdClose() {
		return dClose;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<DealMember> getDealMembers() {
		return dealMembers;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getDirection() {
		return direction;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<Document> getDocuments() {
		return documents;
	}

	@Temporal(TemporalType.DATE)
	public Date getdOpen() {
		return dOpen;
	}

	@Temporal(TemporalType.DATE)
	public Date getdStart() {
		return dStart;
	}

	@Temporal(TemporalType.DATE)
	public Date getdStop() {
		return dStop;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@Column(precision = 12, scale = 3)
	public BigDecimal getMargin() {
		return margin;
	}

	public Double getMarginPersentage() {
		return marginPersentage;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getOrderStage() {
		return orderStage;
	}

	@Transient
	public BigDecimal getPayAdjustment() {
		return payAdjustment;
	}

	@Transient
	public Double getPayAjustmentPersentage() {
		return payAjustmentPersentage;
	}

	@Column(precision = 12, scale = 3)
	public BigDecimal getPayDiscount() {
		return payDiscount;
	}

	public Double getPayDiscountPersentage() {
		return payDiscountPersentage;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<Payment> getPayments() {
		return payments;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getPaymentType() {
		return paymentType;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<Product> getPayProducts() {
		return payProducts;
	}

	@Column(precision = 12, scale = 3)
	public BigDecimal getPaySum() {
		return paySum;
	}

	@Column(precision = 12, scale = 3)
	public BigDecimal getPayTax() {
		return payTax;
	}

	public Double getPayTaxPersentage() {
		return payTaxPersentage;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Right getRight() {
		return right;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getSaleCategory() {
		return saleCategory;
	}

	public Double getSaleChance() {
		return saleChance;
	}

	@Column(precision = 12, scale = 3)
	public BigDecimal getSaleClientHave() {
		return saleClientHave;
	}

	public Boolean getSaleDecline() {
		return saleDecline;
	}

	public Boolean getSaleFrozen() {
		return saleFrozen;
	}

	public Boolean getSaleInProcess() {
		return saleInProcess;
	}

	public Boolean getSaleOver() {
		return saleOver;
	}

	public Boolean getSaleStart() {
		return saleStart;
	}

	@Column(precision = 12, scale = 3)
	public BigDecimal getSaleSum() {
		return saleSum;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getSaleType() {
		return saleType;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getSource() {
		return source;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Account getTeamAccount() {
		return teamAccount;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<TeamMember> getTeamMembers() {
		return teamMembers;
	}

	public void setBills(Set<Bill> bills) {
		this.bills = bills;
	}

	public void setContrAccounts(Set<Account> contrAccounts) {
		this.contrAccounts = contrAccounts;
	}

	public void setContrProducts(Set<Product> contrProducts) {
		this.contrProducts = contrProducts;
	}

	public void setdClose(Date dClose) {
		this.dClose = dClose;
	}

	public void setDealMembers(Set<DealMember> dealMembers) {
		this.dealMembers = dealMembers;
	}

	public void setDirection(Dict direction) {
		this.direction = direction;
	}

	public void setDocuments(Set<Document> documents) {
		this.documents = documents;
	}

	public void setdOpen(Date dOpen) {
		this.dOpen = dOpen;
	}

	public void setdStart(Date dStart) {
		this.dStart = dStart;
	}

	public void setdStop(Date dStop) {
		this.dStop = dStop;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public void setMarginPersentage(Double marginPersentage) {
		this.marginPersentage = marginPersentage;
	}

	public void setOrderStage(Dict orderStage) {
		this.orderStage = orderStage;
	}

	public void setPayAdjustment(BigDecimal payAdjustment) {
		this.payAdjustment = payAdjustment;
	}

	public void setPayAjustmentPersentage(Double payAjustmentPersentage) {
		this.payAjustmentPersentage = payAjustmentPersentage;
	}

	public void setPayDiscount(BigDecimal payDiscount) {
		this.payDiscount = payDiscount;
	}

	public void setPayDiscountPersentage(Double payDiscountPersentage) {
		this.payDiscountPersentage = payDiscountPersentage;
	}

	public void setPayments(Set<Payment> payments) {
		this.payments = payments;
	}

	public void setPaymentType(Dict paymentType) {
		this.paymentType = paymentType;
	}

	public void setPayProducts(Set<Product> payProducts) {
		this.payProducts = payProducts;
	}

	public void setPaySum(BigDecimal paySum) {
		this.paySum = paySum;
	}

	public void setPayTax(BigDecimal payTax) {
		this.payTax = payTax;
	}

	public void setPayTaxPersentage(Double payTaxPersentage) {
		this.payTaxPersentage = payTaxPersentage;
	}

	public void setRight(Right right) {
		this.right = right;
	}

	public void setSaleCategory(Dict saleCategory) {
		this.saleCategory = saleCategory;
	}

	public void setSaleChance(Double saleChance) {
		this.saleChance = saleChance;
	}

	public void setSaleClientHave(BigDecimal saleClientHave) {
		this.saleClientHave = saleClientHave;
	}

	public void setSaleDecline(Boolean saleDecline) {
		this.saleDecline = saleDecline;
	}

	public void setSaleFrozen(Boolean saleFrozen) {
		this.saleFrozen = saleFrozen;
	}

	public void setSaleInProcess(Boolean saleInProcess) {
		this.saleInProcess = saleInProcess;
	}

	public void setSaleOver(Boolean saleOver) {
		this.saleOver = saleOver;
	}

	public void setSaleStart(Boolean saleStart) {
		this.saleStart = saleStart;
	}

	public void setSaleSum(BigDecimal saleSum) {
		this.saleSum = saleSum;
	}

	public void setSaleType(Dict saleType) {
		this.saleType = saleType;
	}

	public void setSource(Dict source) {
		this.source = source;
	}

	public void setTeamAccount(Account teamAccount) {
		this.teamAccount = teamAccount;
	}

	public void setTeamMembers(Set<TeamMember> teamMembers) {
		this.teamMembers = teamMembers;
	}

}
