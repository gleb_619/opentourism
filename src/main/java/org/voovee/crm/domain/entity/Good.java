package org.voovee.crm.domain.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.voovee.crm.domain.sentity.DefaultEntityAssign;

@Entity
public class Good extends DefaultEntityAssign {

	private static final long serialVersionUID = 6971586244142526442L;

	private String code;

	private String codeAlternative;
	private BigDecimal costPurchase;
	private BigDecimal costSelling;

	private Double dDepth;
	private Double dHeight;
	private Double dWeight;
	private Double dWidth;

	private Integer id;
	private String index;

	private BigDecimal margin;
	private Double marginPersentage;
	private Double occupiedSpace;
	private Integer quantity;
	private Integer quantityAvailable;

	private String sku;
	private Dict unit;

	public Good() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Good(Integer id, String code, String codeAlternative, String sku,
			BigDecimal costPurchase, BigDecimal costSelling, BigDecimal margin,
			Double marginPersentage, Integer quantity,
			Integer quantityAvailable, String index, Double dWeight,
			Double dHeight, Double dWidth, Double dDepth, Dict unit,
			Double occupiedSpace) {
		super();
		this.id = id;
		this.code = code;
		this.codeAlternative = codeAlternative;
		this.sku = sku;
		this.costPurchase = costPurchase;
		this.costSelling = costSelling;
		this.margin = margin;
		this.marginPersentage = marginPersentage;
		this.quantity = quantity;
		this.quantityAvailable = quantityAvailable;
		this.index = index;
		this.dWeight = dWeight;
		this.dHeight = dHeight;
		this.dWidth = dWidth;
		this.dDepth = dDepth;
		this.unit = unit;
		this.occupiedSpace = occupiedSpace;
	}

	public Good(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Column(length = 512)
	public String getCode() {
		return code;
	}

	@Column(length = 512)
	public String getCodeAlternative() {
		return codeAlternative;
	}

	public BigDecimal getCostPurchase() {
		return costPurchase;
	}

	public BigDecimal getCostSelling() {
		return costSelling;
	}

	public Double getdDepth() {
		return dDepth;
	}

	public Double getdHeight() {
		return dHeight;
	}

	public Double getdWeight() {
		return dWeight;
	}

	public Double getdWidth() {
		return dWidth;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@Column(length = 512)
	public String getIndex() {
		return index;
	}

	public BigDecimal getMargin() {
		return margin;
	}

	public Double getMarginPersentage() {
		return marginPersentage;
	}

	public Double getOccupiedSpace() {
		return occupiedSpace;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public Integer getQuantityAvailable() {
		return quantityAvailable;
	}

	@Column(length = 512)
	public String getSku() {
		return sku;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getUnit() {
		return unit;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setCodeAlternative(String codeAlternative) {
		this.codeAlternative = codeAlternative;
	}

	public void setCostPurchase(BigDecimal costPurchase) {
		this.costPurchase = costPurchase;
	}

	public void setCostSelling(BigDecimal costSelling) {
		this.costSelling = costSelling;
	}

	public void setdDepth(Double dDepth) {
		this.dDepth = dDepth;
	}

	public void setdHeight(Double dHeight) {
		this.dHeight = dHeight;
	}

	public void setdWeight(Double dWeight) {
		this.dWeight = dWeight;
	}

	public void setdWidth(Double dWidth) {
		this.dWidth = dWidth;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public void setMarginPersentage(Double marginPersentage) {
		this.marginPersentage = marginPersentage;
	}

	public void setOccupiedSpace(Double occupiedSpace) {
		this.occupiedSpace = occupiedSpace;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public void setQuantityAvailable(Integer quantityAvailable) {
		this.quantityAvailable = quantityAvailable;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public void setUnit(Dict unit) {
		this.unit = unit;
	}

}
