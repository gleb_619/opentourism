package org.voovee.crm.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.voovee.crm.domain.sentity.DefaultEntityObject;

@Entity
public class BillingInformation extends DefaultEntityObject {

	private static final long serialVersionUID = -6764806810087368160L;

	private Contact accountantGeneral;

	private String billingInformation;
	private Contact ceo;

	private Integer id;

	public BillingInformation() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BillingInformation(Integer id, Contact ceo,
			Contact accountantGeneral, String billingInformation) {
		super();
		this.id = id;
		this.ceo = ceo;
		this.accountantGeneral = accountantGeneral;
		this.billingInformation = billingInformation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Contact getAccountantGeneral() {
		return accountantGeneral;
	}

	@Column(length = 2000)
	public String getBillingInformation() {
		return billingInformation;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Contact getCeo() {
		return ceo;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setAccountantGeneral(Contact accountantGeneral) {
		this.accountantGeneral = accountantGeneral;
	}

	public void setBillingInformation(String billingInformation) {
		this.billingInformation = billingInformation;
	}

	public void setCeo(Contact ceo) {
		this.ceo = ceo;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
