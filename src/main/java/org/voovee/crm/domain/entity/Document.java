package org.voovee.crm.domain.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.voovee.crm.domain.sentity.DefaultEntityAssign;

@Entity
public class Document extends DefaultEntityAssign {

	private static final long serialVersionUID = -3596088064303892240L;

	private Integer id;

	private Dict condition;
	
	private Deal deal;
	private Date dCloseOn;
	private Date dOpenOn;
	private Date dSignOn;
	private Document parent;

	private Dict paymentPeriod;
	private BigDecimal saleAmount;
	private BigDecimal saleAmountBase;
	private Integer chance;

	private Account supplier;
	private Dict type;

	public Document() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Document(Integer id, Dict condition, Deal deal, Date dCloseOn,
			Date dOpenOn, Date dSignOn, Document parent, Dict paymentPeriod,
			BigDecimal saleAmount, BigDecimal saleAmountBase, Integer chance,
			Account supplier, Dict type) {
		super();
		this.id = id;
		this.condition = condition;
		this.deal = deal;
		this.dCloseOn = dCloseOn;
		this.dOpenOn = dOpenOn;
		this.dSignOn = dSignOn;
		this.parent = parent;
		this.paymentPeriod = paymentPeriod;
		this.saleAmount = saleAmount;
		this.saleAmountBase = saleAmountBase;
		this.chance = chance;
		this.supplier = supplier;
		this.type = type;
	}


	public Document(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getCondition() {
		return condition;
	}

	@Temporal(TemporalType.DATE)
	public Date getdCloseOn() {
		return dCloseOn;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Deal getDeal() {
		return deal;
	}

	@Temporal(TemporalType.DATE)
	public Date getdOpenOn() {
		return dOpenOn;
	}

	@Temporal(TemporalType.DATE)
	public Date getdSignOn() {
		return dSignOn;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	@OneToOne(fetch = FetchType.LAZY)
	public Document getParent() {
		return parent;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getPaymentPeriod() {
		return paymentPeriod;
	}

	
	@Column(precision = 12, scale = 3)
	public BigDecimal getSaleAmount() {
		return saleAmount;
	}


	public void setSaleAmount(BigDecimal saleAmount) {
		this.saleAmount = saleAmount;
	}

	@Column(precision = 12, scale = 3)
	public BigDecimal getSaleAmountBase() {
		return saleAmountBase;
	}


	public void setSaleAmountBase(BigDecimal saleAmountBase) {
		this.saleAmountBase = saleAmountBase;
	}


	public Integer getChance() {
		return chance;
	}


	public void setChance(Integer chance) {
		this.chance = chance;
	}


	@ManyToOne(fetch = FetchType.LAZY)
	public Account getSupplier() {
		return supplier;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	public Dict getType() {
		return type;
	}

	public void setCondition(Dict condition) {
		this.condition = condition;
	}

	public void setdCloseOn(Date dCloseOn) {
		this.dCloseOn = dCloseOn;
	}

	public void setDeal(Deal deal) {
		this.deal = deal;
	}

	public void setdOpenOn(Date dOpenOn) {
		this.dOpenOn = dOpenOn;
	}

	public void setdSignOn(Date dSignOn) {
		this.dSignOn = dSignOn;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setParent(Document parent) {
		this.parent = parent;
	}

	public void setPaymentPeriod(Dict paymentPeriod) {
		this.paymentPeriod = paymentPeriod;
	}


	public void setSupplier(Account supplier) {
		this.supplier = supplier;
	}

	public void setType(Dict type) {
		this.type = type;
	}

}
