package org.voovee.crm.domain.sentity;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.voovee.crm.domain.entity.Company;
import org.voovee.crm.domain.entity.Tag;
import org.voovee.crm.domain.entity.VUser;

@MappedSuperclass
public class DefaultEntityCRM extends DefaultEntityObject {

	private static final long serialVersionUID = -5747056891780424292L;

	private Company company;
	private String description;
	private Set<Tag> tags = new LinkedHashSet<Tag>();
	
	private String title;
	private String title_alternative;

	private VUser uAssignBy;
	private VUser uCreateBy;
	private VUser uDeleteBy;
	private VUser uModifyBy;
	
	private Date dAssignOn;

	public DefaultEntityCRM() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DefaultEntityCRM(String title) {
		super();
		this.title = title;
	}


	public DefaultEntityCRM(Company company, String description, Set<Tag> tags,
			String title, String title_alternative, VUser uAssignBy,
			VUser uCreateBy, VUser uDeleteBy, VUser uModifyBy, Date dAssignOn) {
		super();
		this.company = company;
		this.description = description;
		this.tags = tags;
		this.title = title;
		this.title_alternative = title_alternative;
		this.uAssignBy = uAssignBy;
		this.uCreateBy = uCreateBy;
		this.uDeleteBy = uDeleteBy;
		this.uModifyBy = uModifyBy;
		this.dAssignOn = dAssignOn;
	}

	@ManyToOne
	public Company getCompany() {
		return company;
	}

	@Column(length = 4000)
	public String getDescription() {
		return description;
	}

	@OneToMany(cascade = CascadeType.PERSIST)
	public Set<Tag> getTags() {
		return tags;
	}

	@Column(length = 512)
	public String getTitle() {
		return title;
	}

	@Column(length = 512)
	public String getTitle_alternative() {
		return title_alternative;
	}

	@ManyToOne
	public VUser getuAssignBy() {
		return uAssignBy;
	}

	@ManyToOne
	public VUser getuCreateBy() {
		return uCreateBy;
	}

	@ManyToOne
	public VUser getuDeleteBy() {
		return uDeleteBy;
	}

	@ManyToOne
	public VUser getuModifyBy() {
		return uModifyBy;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setTags(Set<Tag> tags) {
		this.tags = tags;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setTitle_alternative(String title_alternative) {
		this.title_alternative = title_alternative;
	}

	public void setuAssignBy(VUser uAssignBy) {
		this.uAssignBy = uAssignBy;
	}

	public void setuCreateBy(VUser uCreateBy) {
		this.uCreateBy = uCreateBy;
	}

	public void setuDeleteBy(VUser uDeleteBy) {
		this.uDeleteBy = uDeleteBy;
	}

	public void setuModifyBy(VUser uModifyBy) {
		this.uModifyBy = uModifyBy;
	}

	@Temporal(TemporalType.DATE)
	public Date getdAssignOn() {
		return dAssignOn;
	}

	public void setdAssignOn(Date dAssignOn) {
		this.dAssignOn = dAssignOn;
	}

}
