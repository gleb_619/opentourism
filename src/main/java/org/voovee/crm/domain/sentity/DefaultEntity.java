package org.voovee.crm.domain.sentity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.eclipse.persistence.annotations.Index;
import org.voovee.crm.domain.entity.listner.DefaultEntityListner;

@MappedSuperclass
@EntityListeners({ DefaultEntityListner.class })
public class DefaultEntity implements Serializable {

	private static final long serialVersionUID = -1166152444738382422L;

	private String key;
	private Long version;

	public DefaultEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DefaultEntity(String key, Long version) {
		super();
		this.key = key;
		this.version = version;
	}

	@Column(length = 40)
	@Index
	public String getKey() {
		return key;
	}

	@Version
	public Long getVersion() {
		return version;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

}
