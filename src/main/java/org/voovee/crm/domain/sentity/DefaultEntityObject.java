package org.voovee.crm.domain.sentity;

import java.util.Date;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.voovee.crm.domain.entity.listner.DefaultEntityObjectListner;

@MappedSuperclass
@EntityListeners({ DefaultEntityObjectListner.class })
public class DefaultEntityObject extends DefaultEntity {

	private static final long serialVersionUID = 5297670976854818039L;

	private Date dCreateOn;
	private Date dDeleteOn;
	private Date dModifyOn;

	public DefaultEntityObject() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Temporal(TemporalType.DATE)
	public Date getdCreateOn() {
		return dCreateOn;
	}

	@Temporal(TemporalType.DATE)
	public Date getdDeleteOn() {
		return dDeleteOn;
	}

	@Temporal(TemporalType.DATE)
	public Date getdModifyOn() {
		return dModifyOn;
	}

	public void setdCreateOn(Date dCreateOn) {
		this.dCreateOn = dCreateOn;
	}

	public void setdDeleteOn(Date dDeleteOn) {
		this.dDeleteOn = dDeleteOn;
	}

	public void setdModifyOn(Date dModifyOn) {
		this.dModifyOn = dModifyOn;
	}

}
