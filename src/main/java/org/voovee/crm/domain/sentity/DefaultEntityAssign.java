package org.voovee.crm.domain.sentity;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import org.voovee.crm.domain.entity.Account;
import org.voovee.crm.domain.entity.Contact;

@MappedSuperclass
public class DefaultEntityAssign extends DefaultEntityCRM {

	private static final long serialVersionUID = -9162651312350600998L;

	private Account account;
	private Contact contact;

	public DefaultEntityAssign() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DefaultEntityAssign(Account account, Contact contact) {
		super();
		this.account = account;
		this.contact = contact;
	}

	public DefaultEntityAssign(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@ManyToOne
	public Account getAccount() {
		return account;
	}

	@ManyToOne
	public Contact getContact() {
		return contact;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

}
